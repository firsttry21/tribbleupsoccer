﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using System.IO;

public class PlayerDataManager : MonoBehaviour
{
    public static PlayerDataManager instance { get; private set; }

    //TODO : this data is to be initialised on LOGIN
    public UserInfo                 userInfo              { get; private set; }
    public int                      selectedProgramId     { get; private set; }
    public DTO_WorkoutProgram[]     completedProgramsData { get; private set; }
    public int                      jugglesHighScore      { get; private set; }
    public int                      streakNumberDays      { get; private set; }
    public Dictionary<int, int>     drillsPersonalBest    { get; private set; }
    public List<DTO_WorkoutProgram> programsHistory       { get; private set; }

    private DateTime lastServerSaveTime;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    private void Start()
    {
        //programsHistory = loadAllProgramHistory();
        //completedProgramsData = loadAllCompletedProgramData();

        //SceneManager.LoadScene("MainMenuScene");
    }

    private void OnApplicationPause(bool pause)
    {
        //if (pause)
        //{
        //    //if (lastServerSaveTime != null)
        //    //{
        //    //    TimeSpan timeBetweenServerSaves = DateTime.Now - lastServerSaveTime;

        //    //    if (timeBetweenServerSaves.TotalMinutes > 3) //only save to server onapplicationpause if 3 minutes have passed since last server save
        //    //    {
        //    if (GameManager.instance == null ||
        //       (GameManager.instance != null && GameManager.instance.state == GameManager.GameState.Finished))
        //    {
        //        DTO_PlayerData playerData = new DTO_PlayerData();
        //        playerData.selectedProgramId = this.selectedProgramId;
        //        playerData.programsData = this.completedProgramsData;
        //        playerData.programHistoryData = this.programsHistory;
        //        playerData.drillsPersonalBest = this.drillsPersonalBest;
        //        playerData.jugglesHighScore = this.jugglesHighScore;
        //        playerData.streakNumberDays = this.streakNumberDays;



        //        string debug_playerDataJson = JsonConvert.SerializeObject(playerData, Formatting.Indented);
        //        File.WriteAllText(GlobalValue.saveFilePath, debug_playerDataJson);
        //        //SoccerWebApi.instance.sendPlayerDataToServer(playerData); 

        //        lastServerSaveTime = DateTime.Now;
        //    }
        //    //    }
        //    //}
        //}
    }

    public void onPlayerSignout()
    {
        if (MainMenuManager.instance != null)
            MainMenuManager.instance.showHideLoadingScreen(true);

        PlayerPrefs.DeleteKey(GlobalValue.loginStatusKey);
        PlayerPrefs.DeleteKey(GlobalValue.currentPlayerUsernameKey);
        PlayerPrefs.DeleteKey(GlobalValue.currentPlayerPasswordKey);

        setPlayerData(null);

        SceneManager.LoadSceneAsync("LoginSignupScene");
    }

    public void setPlayerData(DTO_PlayerData data)
    {
        if (data == null)
            return;

        PlayerPrefs.SetInt(GlobalValue.selectedWorkoutProgramKey, data.selectedProgramId);
        completedProgramsData = WorkoutProgramHolder.instance.getAllProgramsData();
        #region Needs optimisation
        for (int i = 0; i < data.activePrograms.Length; i++)
        {
            for (int j = 0; j < completedProgramsData.Length; j++)
            {
                if (completedProgramsData[j].programId == data.activePrograms[i].programId - 1) //TODO WTF is this shit
                {
                    completedProgramsData[j].daysCompleted = data.activePrograms[i].daysCompleted;
                }
            }
        }
        #endregion
        programsHistory = data.programHistoryData;
        drillsPersonalBest = data.drillsPersonalBest;
        jugglesHighScore = data.jugglesHighScore;
        streakNumberDays = data.streakNumberDays;
    }

    public void saveCompletedProgram(DTO_WorkoutProgram program)
    {
        local_updateCompletedProgramsDataWith(program);
        local_updateProgramsHistoryWith(program);

        attemptUploadProgramData(program);
    }

    private void attemptUploadProgramData(DTO_WorkoutProgram program)
    {
        string previousCacheJson = PlayerPrefs.GetString(GlobalValue.uploadCacheDataKey, String.Empty);
        ProgramServerCache programDataToUpload = JsonConvert.DeserializeObject<ProgramServerCache>(previousCacheJson);
        //  if theres already some data queued to be uploaded
        if (programDataToUpload != null && programDataToUpload.unsavedPrograms != null)
        {
            if (programDataToUpload.unsavedPrograms.Count != 0)
            {
                //  update it with new data
                programDataToUpload.familyId = PlayerPrefs.GetInt(GlobalValue.selectedPlayerIdKey, 0);
                programDataToUpload.selectedProgramId = PlayerPrefs.GetInt(GlobalValue.selectedWorkoutProgramKey, 0);
                programDataToUpload.juggleHighScore = PlayerPrefs.GetInt(GlobalValue.maxJugglesKey, 0);
                programDataToUpload.unsavedPrograms.Add(program);
                //  and prepare for it to be uploaded
                PlayerPrefs.SetString(GlobalValue.uploadCacheDataKey, JsonConvert.SerializeObject(programDataToUpload));
            }
        }
        else
        {
            // else just start data upload queue
            programDataToUpload = new ProgramServerCache(
            PlayerPrefs.GetInt(GlobalValue.selectedPlayerIdKey, 0),
            PlayerPrefs.GetInt(GlobalValue.selectedWorkoutProgramKey, 0),
            PlayerPrefs.GetInt(GlobalValue.maxJugglesKey, 0),
            new List<DTO_WorkoutProgram>() { program });

            PlayerPrefs.SetString(GlobalValue.uploadCacheDataKey, JsonConvert.SerializeObject(programDataToUpload));
        }
        SoccerWebApi.instance.postProgramDataUploadRequest(programDataToUpload);
    }

    public void onPlayerDataUploadSucceed()
    {
        ProgramServerCache.clearData();
    }

    public void onPlayerDataUploadFail() { }

    private void local_updateProgramsHistoryWith(DTO_WorkoutProgram program)
    {
        if (programsHistory == null)
            programsHistory = new List<DTO_WorkoutProgram>();

        programsHistory.Add(program);
        //TODO also save to server here

        string json = JsonConvert.SerializeObject(programsHistory);
        PlayerPrefs.SetString(GlobalValue.programsHistoryKey, json);
        Debug.Log("json=" + json);
    }

    private void local_updateCompletedProgramsDataWith(DTO_WorkoutProgram program)
    {
        completedProgramsData[program.programId] = (DTO_WorkoutProgram)program;
        string json = JsonConvert.SerializeObject(completedProgramsData);
        PlayerPrefs.SetString(GlobalValue.completedProgramsDataKey, json);
    }

    public List<DTO_WorkoutProgram> loadAllProgramHistory()
    {
        if (programsHistory != null)
        {
            return programsHistory;
        }
        else
        {
            string json = PlayerPrefs.GetString(GlobalValue.programsHistoryKey, string.Empty);
            if (json != string.Empty)
            {
                List<DTO_WorkoutProgram> progressFromJson = JsonConvert.DeserializeObject<List<DTO_WorkoutProgram>>(json);
                Debug.Log("progressFromJson:\nCount=" + progressFromJson.Count);
                return progressFromJson;
            }
            else
                return new List<DTO_WorkoutProgram>();
        }
    }

    private DTO_WorkoutProgram[] loadAllcompletedProgramsData()
    {
        string json = PlayerPrefs.GetString(GlobalValue.completedProgramsDataKey, string.Empty);
        if (json != string.Empty)
        {
            DTO_WorkoutProgram[] completedProgramsSavedData = JsonConvert.DeserializeObject<DTO_WorkoutProgram[]>(json);
            return completedProgramsSavedData;
        }
        else
        {
            var defaultProgramsList = new List<DTO_WorkoutProgram>();
            foreach (var program in WorkoutProgramHolder.instance.getAllPrograms())
            {
                defaultProgramsList.Add((DTO_WorkoutProgram)program);
            }
            return defaultProgramsList.ToArray();
        }
    }

    public DTO_WorkoutProgram getProgramDataById(int programId)
    {
        if (!(programId >= completedProgramsData.Length))
        {
            return completedProgramsData[programId];
        }
        else
        {
            throw new ArgumentOutOfRangeException("programId", String.Format("programId={0} && programsCount={1}", programId, completedProgramsData.Length));
        }
    }

    public FamilyMember[] getCurrentPlayerFamilyMembers()
    {
        return userInfo.membersList.ToArray();
    }

    public void setUserInfo(UserInfo info)
    {
        this.userInfo = info;
    }
}
