﻿using System;
using System.Runtime.Serialization;

[Serializable]
[DataContract]
public class DTO_WorkoutProgram
{
    [DataMember(Name = "programId")]          public int            programId;
    [DataMember(Name = "programName")]        public string         programTitle;
    [DataMember(Name = "drills")]             public WorkoutDrill[] workoutDrills;
    [DataMember(Name = "totalRepsCompleted")] public int            totalCompletedReps;
    [DataMember(Name = "daysCompleted")]      public int            daysCompleted;
    [DataMember(Name = "lastCompletedTime")]  public DateTime       dateTimeWhenCompleted;
                                              public int            totalDays;
                                              //public string         dateWhenCompleted;
                                              //public string         timeWhenCompleted;

    public static explicit operator WorkoutProgram(DTO_WorkoutProgram program) =>
        new WorkoutProgram()
        {
            programId = program.programId,
            programTitle = program.programTitle,
            totalDays = program.totalDays,
            workoutDrills = program.workoutDrills,
            totalCompletedReps = program.totalCompletedReps,
            daysCompleted = program.daysCompleted,
            //dateWhenCompleted = program.dateWhenCompleted,
            //timeWhenCompleted = program.timeWhenCompleted
        };
}
