﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

[Serializable] [DataContract]
public class DTO_PlayerData
{
    public UserInfo userInfo;
    [DataMember(Name = "selectedProgramId")] public int selectedProgramId;
    [DataMember(Name = "programs")] public DTO_WorkoutProgram[] activePrograms;
    public int jugglesHighScore;
    //[DataMember(Name = "streakNumberDays")] 
    public int streakNumberDays;
    public Dictionary<int, int> drillsPersonalBest;
    [DataMember(Name = "playerHistory")]public List<DTO_WorkoutProgram> programHistoryData;
}
