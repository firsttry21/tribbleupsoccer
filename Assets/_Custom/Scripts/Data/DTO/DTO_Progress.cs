﻿using System;
using System.Collections.Generic;

[Serializable]
public class DTO_Progress
{
    public SortedDictionary<string, Stack<DTO_WorkoutProgram>> progressByDate = new SortedDictionary<string, Stack<DTO_WorkoutProgram>>();

    public DTO_Progress() { }

    public DTO_Progress(SortedDictionary<string, Stack<DTO_WorkoutProgram>> progress)
    {
        this.progressByDate = progress;
    }
}
