﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
[DataContract]
public class ProgramServerCache
{
    public int familyId;
    public int selectedProgramId;
    public int juggleHighScore;
    public List<DTO_WorkoutProgram> unsavedPrograms = new List<DTO_WorkoutProgram>();

    public ProgramServerCache(int id, int selectionId, int maxJuggles, List<DTO_WorkoutProgram> programs)
    {
        familyId = id; selectedProgramId = selectionId; juggleHighScore = maxJuggles; unsavedPrograms = programs;
    }

    public static void clearData()
    {
        PlayerPrefs.DeleteKey(GlobalValue.uploadCacheDataKey);
    }
}
