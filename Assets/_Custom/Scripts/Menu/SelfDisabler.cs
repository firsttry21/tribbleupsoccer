﻿
using UnityEngine;
using UnityEngine.UI;

public class SelfDisabler : MonoBehaviour
{
    public int indexToDisable = 1;
    void Start()
    {
        if (transform.GetSiblingIndex() == indexToDisable)
        {
            GetComponent<Toggle>().interactable = false;
        }
    }
}
