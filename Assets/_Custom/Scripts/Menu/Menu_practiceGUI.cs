﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Menu_practiceGUI : MonoBehaviour
{
    public Text totalScoreText;
    public Text gainedScoreText;
    public GameObject hitTargetsMessage;

    private void OnEnable()
    {
        hitTargetsMessage.SetActive(true);
    }

    private void Update()
    {
        totalScoreText.text = "/" + LevelManager.instance.selectedDrill.totalReps.ToString("00");
        gainedScoreText.text = GameManager.instance.currentDrillCompletedReps.ToString("00");
    }
}