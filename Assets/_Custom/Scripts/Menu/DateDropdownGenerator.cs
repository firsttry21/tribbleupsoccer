﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DateDropdownGenerator : MonoBehaviour
{
    public Dropdown monthDropdown;
    public Dropdown dayDropdown;
    public Dropdown yearDropdown;

    private int selectedYear;
    private int selectedMonth;
    private int selectedDay;

    List<string> years = new List<string> { "Select Year" };
    List<string> days = new List<string>() { "Select Day" };
    List<string> months = new List<string> { "Select Month",
                                             "January",
                                             "February",
                                             "March",
                                             "April",
                                             "May",
                                             "June",
                                             "July",
                                             "August",
                                             "September",
                                             "October",
                                             "November",
                                             "December"};

    private void OnEnable()
    {
        selectedYear = 0;
        selectedMonth = 0;
        selectedDay = 0;

        int defaultYear = DateTime.Now.Year;
        int defaultMonth = 1;
        //clear all dropdowns at start
        foreach (Dropdown dd in GetComponentsInChildren<Dropdown>(true))
        {
            dd.ClearOptions();
        }

        for (int i = defaultYear - 1; i >= (defaultYear - 50); i--)
        {
            years.Add(i.ToString());
        }

        int daysCount = DateTime.DaysInMonth(defaultYear, defaultMonth);
        for (int i = 1; i <= daysCount; i++)
        {
            days.Add(i.ToString("00"));
        }

        monthDropdown.AddOptions(months);
        yearDropdown.AddOptions(years);
        dayDropdown.AddOptions(days);
    }

    public void updateDaysDropDown()
    {
        if (selectedYear == 0 || selectedMonth == 0)
            return;

        dayDropdown.ClearOptions();
        days.RemoveRange(1, days.Count - 1);

        int daysCount = DateTime.DaysInMonth(selectedYear, selectedMonth);
        for (int i = 1; i <= daysCount; i++)
        {
            days.Add(i.ToString("00"));
        }
        dayDropdown.AddOptions(days);

        if (selectedDay > daysCount)
            selectedDay = dayDropdown.value = daysCount;
        else
            dayDropdown.value = selectedDay;

        //dayDropdown.value = 0;
        //selectedDay = 0;
    }

    public void setSelectedBirthday()
    {
        try
        {
            selectedYear = Convert.ToInt32(yearDropdown.options[yearDropdown.value].text);
        }
        catch (FormatException fe)
        {
            Debug.LogError("Format Exception for value " + selectedYear + ": " + fe.Message);
            selectedYear = 0;
        }
        selectedMonth = monthDropdown.value;
        selectedDay = dayDropdown.value;
        Debug.Log("SELECTED BIRTHDAY: " + selectedYear + "-" + selectedMonth + "-" + selectedDay);
    }

    public DateTime? getSelectedBirthday()
    {
        if (selectedYear == 0 || selectedMonth == 0 || selectedDay == 0)
            return null;
        else
            return new DateTime(selectedYear, selectedMonth, selectedDay);
    }
}
