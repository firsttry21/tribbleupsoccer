﻿using System;
using UnityEngine;
using UnityEngine.UI;
using RectExtension;

public class Menu_BallScanningScript : MonoBehaviour
{
    public RectTransform detectorCircle;
    public bool temp_isBallInCrosshair;
    public RectTransform crosshair;
    public Color crosshairDefaultColor = Color.white;
    public Color crosshairDetectionColor = Color.green;
    public float completeScanAfter = 1f;

    float timer;

    Image[] crosshairCorners;

    private void Awake()
    {
        crosshairCorners = crosshair.GetComponentsInChildren<Image>();
        setCrosshairColor(crosshairDefaultColor);

        temp_isBallInCrosshair = false;
    }

    private void Update()
    {
        if (/*isBallInsideCrosshair()*/temp_isBallInCrosshair)
        {
            setCrosshairColor(crosshairDetectionColor);
            timer += Time.deltaTime;
            if (timer >= completeScanAfter)
            {
                switch (GameManager.instance.gameMode)
                {
                    case GameManager.GameMode.Workout:
                        GameManager.instance.startWorkoutDrill();
                        break;
                    case GameManager.GameMode.Practice:
                        GameManager.instance.startPracticeDrill();
                        break;
                    case GameManager.GameMode.Juggling:
                        GameManager.instance.startJuggleMode();
                        break;
                    default:
                        Debug.LogError("FUCK THE DEVS!!!");
                        break;
                }
            }
        }
        else
        {
            setCrosshairColor(crosshairDefaultColor);
            timer = 0;
        }
    }

    public void temp_skipScan()
    {
        temp_isBallInCrosshair = true;
    }

    private void setCrosshairColor(Color colorToSet)
    {
        foreach (Image corner in crosshairCorners)
        {
            corner.color = colorToSet;
        }
    }

    private bool isBallInsideCrosshair()
    {
        return detectorCircle.isRectOverlappingCheckByDistance(crosshair);
    }
}