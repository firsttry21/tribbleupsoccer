﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class menu_pauseScreen : MonoBehaviour
{
    public GameObject nextDrillBtn;
    public GameObject replayDrillBtn;
    public GameObject endWorkoutBtn;

    private void OnEnable()
    {
        endWorkoutBtn.GetComponent<Button>().onClick.RemoveAllListeners();

        switch (GameManager.instance.gameMode)
        {
            case GameManager.GameMode.Workout:
                endWorkoutBtn.GetComponent<Button>().onClick.AddListener(() => MenuManager.instance.btn_endWorkout());
                break;
            case GameManager.GameMode.Practice:
                endWorkoutBtn.GetComponent<Button>().onClick.AddListener(() => MenuManager.instance.btn_endPractice());
                break;
            case GameManager.GameMode.Juggling:
                endWorkoutBtn.GetComponent<Button>().onClick.AddListener(() => MenuManager.instance.btn_endJuggling());
                break;
            default:
                break;
        }

        if (GameManager.instance.gameMode == GameManager.GameMode.Juggling)
        {
            nextDrillBtn.SetActive(false);
            replayDrillBtn.SetActive(false);
            endWorkoutBtn.SetActive(true);
            return;
        }
        if (LevelManager.instance.getDrillsLeftMessage() == "LAST DRILL" ||
            LevelManager.instance.getDrillsLeftMessage() == "PRACTICE")
        {
            nextDrillBtn.SetActive(false);
            endWorkoutBtn.SetActive(true);
            return;
        }
        else
        {
            endWorkoutBtn.SetActive(false);
            nextDrillBtn.SetActive(true);
        }
    }
}