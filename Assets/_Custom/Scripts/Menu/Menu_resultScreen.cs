﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Menu_resultScreen : MonoBehaviour
{
    public GameObject drillResultEntryPrefab;
    public Transform content;
    public Transform header;
    public Transform footer;
    [Space] public Text subtitleText;
    public Transform stars;
    public Color starOnColor = Color.yellow;
    public Color starOffColor = Color.gray;
    [Space] public Text summaryDrillsText;
    public Text summaryMinutesText;
    public Text summaryRepsText;

    int totalCompletedDrills;
    float totalCompletedMinutes;
    int totalCompletedReps;

    internal void showResultFor(DTO_WorkoutProgram program)
    {
        var oldResultEntries = GetComponentsInChildren<Menu_DrillResultEntry>();
        foreach (var entry in oldResultEntries)
        {
            Destroy(entry.gameObject);
        }
        totalCompletedDrills = 0;
        totalCompletedMinutes = 0;
        totalCompletedReps = 0;

        subtitleText.text = "DAY " + program.daysCompleted + " • " + program.programTitle;
        for (int i = 0; i < program.workoutDrills.Length; i++)
        {
            var resultEntry = GameObject.Instantiate(drillResultEntryPrefab, content);
            var resultEntryScript = resultEntry.GetComponent<Menu_DrillResultEntry>();
            resultEntryScript.drillNameText.text = program.workoutDrills[i].drillName;
            resultEntryScript.drillHighScoreText.text = "Personal Best: " + program.workoutDrills[i].personalBest.ToString("00") + " Reps";
            resultEntryScript.completedRepsText.text = "<b><size=22>" + program.workoutDrills[i].repsCompleted.ToString("00") + "</size></b>\n<size=10>REPS</size>";
            resultEntryScript.drillProgressSlider.minValue = 0;
            resultEntryScript.drillProgressSlider.maxValue = program.workoutDrills[i].totalReps;
            resultEntryScript.drillProgressSlider.value = program.workoutDrills[i].repsCompleted;
            resultEntryScript.sliderCompletedText.text = program.workoutDrills[i].repsCompleted.ToString("00");
            resultEntryScript.sliderGoalText.text = program.workoutDrills[i].totalReps.ToString("00");
        }

        header.SetAsFirstSibling();
        footer.SetAsLastSibling();

        totalCompletedDrills = program.workoutDrills.Length;
        foreach (var p in program.workoutDrills)
        {
            totalCompletedMinutes += p.timeCompleted;
            totalCompletedReps += p.repsCompleted;
        }

        summaryDrillsText.text = totalCompletedDrills.ToString();
        summaryMinutesText.text = Mathf.RoundToInt(totalCompletedMinutes / 60).ToString();
        summaryRepsText.text = totalCompletedReps.ToString();
    }
}