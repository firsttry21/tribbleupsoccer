﻿using UnityEngine;
using UnityEngine.UI;

public class Menu_GUI : MonoBehaviour
{
    public Text totalScoreText;
    public Text gainedScoreText;
    public Text timerText;
    public GameObject hitTargetsMessage;

    private void OnEnable()
    {
        hitTargetsMessage.SetActive(true); 
    }

    private void Update()
    {
        totalScoreText.text = "/" + LevelManager.instance.drillsForSelectedProgram[LevelManager.instance.currentDrillIndex].totalReps.ToString("00");
        gainedScoreText.text = GameManager.instance.currentDrillCompletedReps.ToString("00");
        timerText.text = ":" + LevelManager.instance.currentTimer.ToString("00");
    }

    public void temp_giveFreeScore()
    {
        GameManager.instance.addWorkoutScore(1);
    }

}