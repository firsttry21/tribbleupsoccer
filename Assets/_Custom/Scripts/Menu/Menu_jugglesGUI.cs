﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Menu_jugglesGUI : MonoBehaviour
{
    public Text scoreText;
    public Text goalText;
    [Space]
    public int scoreToAchieve = 4;
    public int nextGoalStep = 5;

    private void OnEnable()
    {
        goalText.text = "/" + scoreToAchieve.ToString("00");
    }

    private void Update()
    {
        scoreText.text = GameManager.instance.gainedJuggleScore.ToString("00");
    }
}