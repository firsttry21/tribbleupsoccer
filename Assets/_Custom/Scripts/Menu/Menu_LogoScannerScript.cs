﻿using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Menu_LogoScannerScript : MonoBehaviour, ITrackableEventHandler
{
    public bool isLogoDetected { get; private set; }

    TrackableBehaviour trackableBehaviour;

    private void Start()
    {
        trackableBehaviour = FindObjectOfType<TrackableBehaviour>();
        if (trackableBehaviour != null) trackableBehaviour.RegisterTrackableEventHandler(this);
    }

    private void OnDestroy()
    {
        if (trackableBehaviour != null)
        {
            trackableBehaviour.UnregisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        //Debug.Log("============S T A T E  C H A N G E D============");
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            isLogoDetected = true;
            Debug.Log("<><><><><><><><><><><><><><><> LOGO DETECTED <><><><><><><><><><><><><><><><><>");
        }
        else
        {
            isLogoDetected = false;
        }
    }
}