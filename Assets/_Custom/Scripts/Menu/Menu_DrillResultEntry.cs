﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Menu_DrillResultEntry : MonoBehaviour
{
    public Text drillNameText;
    public Text drillHighScoreText;
    public Text completedRepsText;
    public Slider drillProgressSlider;
    public Text sliderCompletedText;
    public Text sliderGoalText;
}