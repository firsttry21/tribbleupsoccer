﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Mainmenu_Practice : MonoBehaviour
{
    public Transform drillsContentTransform;
    public GameObject practiceSelectPrefab;
    public MainMenu_DrillPreview selectedDrillPreview;

    [HideInInspector] public Image lastSelectedBg { get; set; }
    [HideInInspector] public Text lastSelectedText { get; set; }

    WorkoutDrill[] allDrills;

    private void Start()
    {
        foreach (Transform t in drillsContentTransform.transform)
            Destroy(t.gameObject);

        allDrills = WorkoutProgramHolder.instance.getAllDrills();
        WorkoutDrill[] allDrillsDiscrete = allDrills.OrderBy(x => x.drillName).GroupBy(x => x.drillName).Select(y => y.First()).ToArray();

        for (int i = 0; i < allDrillsDiscrete.Length; i++)
        {
            var current = GameObject.Instantiate(practiceSelectPrefab, drillsContentTransform);
            var currentSelectorScript = current.GetComponent<Mainmenu_PracticeDrillSelector>();
            currentSelectorScript.drillTitleText.text = allDrillsDiscrete[i].drillName;
            currentSelectorScript.associatedDrillId = allDrillsDiscrete[i].drillId;
        }

        setSelectedPracticeDrill(PlayerPrefs.GetInt(GlobalValue.selectedPracticeDrillId, 0));
    }

    public void setSelectedPracticeDrill(int id)
    {
        if (allDrills == null) return;

        PlayerPrefs.SetInt(GlobalValue.selectedPracticeDrillId, id);
        selectedDrillPreview.drillNameText.text = allDrills[id].drillName;
        //TODO also change preview gif etc here
    }
}