﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu_Program : MonoBehaviour
{
    public int programId;
    public bool isCurrent;
    public string programTitle;
    public string programRemarks;
    public string programDifficulty;
    public int totalDays;
    [HideInInspector]
    public int daysCompleted;
    public Button selectWorkoutBtn;
    public Button resetWorkoutBtn;
    public Text programTitleText;
    public Text programRemarksText;
    public Text programSpecsText;

    MainMenu_ProgramSelection programSelection;

    private void Awake()
    {
        programSelection = GetComponentInParent<MainMenu_ProgramSelection>();
    }

    void Start()
    {
        programTitleText.text = programTitle;
        programRemarksText.text = programRemarks;
        programSpecsText.text = programDifficulty.ToUpper() + " • " + totalDays.ToString() + " DAYS • DAY " + PlayerDataManager.instance.getProgramDataById(programId).daysCompleted;
    }

    void Update()
    {
        if (isCurrent)
        {
            selectWorkoutBtn.gameObject.SetActive(false);
            resetWorkoutBtn.gameObject.SetActive(true);
        }
        else
        {
            resetWorkoutBtn.gameObject.SetActive(false);
            selectWorkoutBtn.gameObject.SetActive(true);
        }
    }

    public void btn_selectWorkout()
    {
        WorkoutProgramHolder.instance.setSelectedProgram(programId);

        programSelection.onProgramSelect(this.transform);
    }
}