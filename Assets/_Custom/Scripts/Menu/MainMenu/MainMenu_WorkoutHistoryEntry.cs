﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu_WorkoutHistoryEntry : MonoBehaviour
{
    public Text dayCounterText;
    public Text workoutTitleText;
    public Text timeWhenCompletedText;
    public Text totalRepsCompletedText;
    public DTO_WorkoutProgram associatedWorkoutProgram;

    public void setEntryData(DTO_WorkoutProgram programDefaults, DTO_WorkoutProgram programEntry)
    {
        //set program's data to show when clicked on its entry
        associatedWorkoutProgram = programDefaults;
        associatedWorkoutProgram.totalCompletedReps = programEntry.totalCompletedReps;
        associatedWorkoutProgram.daysCompleted = programEntry.daysCompleted;
        associatedWorkoutProgram.dateTimeWhenCompleted = programEntry.dateTimeWhenCompleted;
        if (programEntry.workoutDrills != null)      // TODO THIS IS ONLY FOR UNTILL FETCHING DRILLS FROM SERVER ISNT FIXED!!!
        {
            for (int i = 0; i < associatedWorkoutProgram.workoutDrills.Length; i++)
            {
                associatedWorkoutProgram.workoutDrills[i].detectorPosX = programEntry.workoutDrills[i].detectorPosX;
                associatedWorkoutProgram.workoutDrills[i].repsCompleted = programEntry.workoutDrills[i].repsCompleted;
                associatedWorkoutProgram.workoutDrills[i].timeCompleted = programEntry.workoutDrills[i].timeCompleted;
                associatedWorkoutProgram.workoutDrills[i].personalBest = programEntry.workoutDrills[i].personalBest;
            } 
        }
        dayCounterText.text = "Day " + programEntry.daysCompleted.ToString();
        workoutTitleText.text = programEntry.programTitle;
        timeWhenCompletedText.text = programEntry.dateTimeWhenCompleted.ToString("hh:mm tt").ToUpper();
        totalRepsCompletedText.text = programEntry.totalCompletedReps.ToString() + " REPS";
    }

    public void btn_showResultHistoryForWorkout()
    {
        var clickedProgramEntry = EventSystem.current.currentSelectedGameObject.GetComponentInParent<MainMenu_WorkoutHistoryEntry>().associatedWorkoutProgram;
        MainMenuManager.instance.startResultScreenForProgram(clickedProgramEntry);
    }
}
