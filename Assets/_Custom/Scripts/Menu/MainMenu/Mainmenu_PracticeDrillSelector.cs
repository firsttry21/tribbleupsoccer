﻿using UnityEngine;
using UnityEngine.UI;

public class Mainmenu_PracticeDrillSelector : MonoBehaviour
{
    public Text drillTitleText;
    [HideInInspector]
    public int associatedDrillId;
    [Space]
    public Color colorOnSelection = Color.blue;
    public Color colorOnUnselected = Color.clear;

    private void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(() => { pickPracticeDrill(associatedDrillId); });
    }

    void pickPracticeDrill(int id)
    {
        var parent = GetComponentInParent<Mainmenu_Practice>();

        if (parent != null)
        {
            if (parent.lastSelectedBg != null && parent.lastSelectedText != null)
            {
                parent.lastSelectedBg.color = colorOnUnselected;
                parent.lastSelectedText.color = Color.black;
            }

            parent.setSelectedPracticeDrill(associatedDrillId);

            var clickedDrillBg = GetComponent<Image>();
            var clickedDrillText = GetComponentInChildren<Text>();
            clickedDrillBg.color = colorOnSelection;
            clickedDrillText.color = Color.white;

            parent.lastSelectedBg = clickedDrillBg;
            parent.lastSelectedText = clickedDrillText;
        }
    }
}