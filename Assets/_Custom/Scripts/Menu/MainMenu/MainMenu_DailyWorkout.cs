﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu_DailyWorkout : MonoBehaviour
{
    public Text workoutTitle;
    public Text completedDaysText;
    public Text totalDaysText;
    public Slider daysProgressSlider;
    public Text startBtnText;
    public GameObject programCompletedPanel;

    private DTO_WorkoutProgram selectedProgram;

    void OnEnable()
    {
        //TODO (1) load selected workout program data from workout program holder
        selectedProgram = PlayerDataManager.instance.getProgramDataById(PlayerPrefs.GetInt(GlobalValue.selectedWorkoutProgramKey, 0));

        workoutTitle.text = selectedProgram.programTitle;
        completedDaysText.text = "Day " + (selectedProgram.daysCompleted).ToString();
        daysProgressSlider.value = selectedProgram.daysCompleted;
        totalDaysText.text = "Day " + selectedProgram.totalDays.ToString();
        daysProgressSlider.maxValue = selectedProgram.totalDays;
        startBtnText.text = "Start Day " + (selectedProgram.daysCompleted + 1).ToString() + " Workout";

        var drillScroller = GetComponentInChildren<MainMenu_DrillScroller>();
        //destory b4 instantiating====================================================================================================
        foreach (Transform t in drillScroller.transform)
        {
            Destroy(t.gameObject);
        }
        foreach (WorkoutDrill drill in selectedProgram.workoutDrills)
        {
            var current = GameObject.Instantiate(drillScroller.drillPreviewPrefab, drillScroller.transform);
            current.GetComponent<MainMenu_DrillPreview>().drillNameText.text = drill.drillName;
        }

        StartCoroutine(scrollDrillsViewToStartCo(drillScroller));
    }

    IEnumerator scrollDrillsViewToStartCo(MainMenu_DrillScroller scroller)
    {
        yield return new WaitForEndOfFrame();
        scroller.GetComponentInParent<ScrollRect>().horizontalNormalizedPosition = 0f;

    }


}