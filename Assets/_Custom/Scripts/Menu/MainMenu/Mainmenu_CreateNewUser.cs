﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class Mainmenu_CreateNewUser : MonoBehaviour
{
    public InputField newUsernameInput;
    public Text newUserInputErrorText;
    private readonly string usernamePattern = "^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"; // username pattern

    private void Start()
    {
        newUserInputErrorText.text = string.Empty;
    }

    public void btn_createNewUser()
    {
        if (newUsernameInput.text.Length < 6)
        {
            newUserInputErrorText.text = "Username too short. Please enter alteast 6 characters.";
            return;
        }
        if (!Regex.IsMatch(newUsernameInput.text, usernamePattern))
        {
            newUserInputErrorText.text = "Please enter a valid username.";
            return;
        }
        FamilyMember[] presentMembers = PlayerDataManager.instance.getCurrentPlayerFamilyMembers();
        if (presentMembers != null)
        {
            for (int i = 0; i < presentMembers.Length; i++)
            {
                if (newUsernameInput.text == presentMembers[i].memberName)
                {
                    newUserInputErrorText.text = "Username already taken.";
                    return;
                }
            }
        }

        MainMenuManager.instance.showHideLoadingScreen(true);
        SoccerWebApi.instance.postCreateNewFamilyMemberRequest(newUsernameInput.text, onCreateNewFamilyMemberSuccess, onCreateNewFamilyMemberFail);
    }

    private void onCreateNewFamilyMemberSuccess()
    {
        this.gameObject.SetActive(false);
        MainMenuManager.instance.showHideLoadingScreen(false);
        MainMenuManager.instance.startMemberSelectionScreen();
    }

    private void onCreateNewFamilyMemberFail(string errMsg)
    {
        MainMenuManager.instance.showHideLoadingScreen(false);
        newUserInputErrorText.text = errMsg;
    }
}
