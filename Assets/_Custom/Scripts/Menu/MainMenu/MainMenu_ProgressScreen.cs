﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Populate progress screen with player's workout history.
/// Load the table on start.
/// Check for allPrograms count change in OnEnable and refresh if data was changed
/// </summary>
public class MainMenu_ProgressScreen : MonoBehaviour
{
    public Transform content;

    public GameObject dateSeparatorPrefab;
    public GameObject workoutProgressEntryPrefab;

    //SortedDictionary<string, Stack<DTO_WorkoutProgram>> workoutProgramsHistoryData;

    List<DTO_WorkoutProgram> workoutProgramsHistoryData;
    string previousDate = String.Empty;

    private void OnEnable()
    {
        if (workoutProgramsHistoryData != null && !workoutProgramsHistoryData.Equals(PlayerDataManager.instance.loadAllProgramHistory()))
        {
            Debug.Log("Reloading Data...");
            Start();
        }
    }

    private void Start()
    {
        workoutProgramsHistoryData = PlayerDataManager.instance.loadAllProgramHistory(); // TODO (6) bring history from server loaded data here

        foreach (Transform t in content)
        {
            Destroy(t.gameObject);
        }

        Debug.Log("historyCount=" + workoutProgramsHistoryData.Count);
        for (int currentIndex = (workoutProgramsHistoryData.Count - 1); currentIndex >= 0; currentIndex--)
        {
            if (!previousDate.Equals(workoutProgramsHistoryData[currentIndex].dateTimeWhenCompleted.ToString("dddd MMMM dd")))
            {
                previousDate = workoutProgramsHistoryData[currentIndex].dateTimeWhenCompleted.ToString("dddd MMMM dd");

                //instantiate date separator
                var dateSeparator = GameObject.Instantiate(dateSeparatorPrefab, content).GetComponent<Text>();
                dateSeparator.text = " - " + previousDate + " - ";
            }

            var workoutHistoryEntry = GameObject.Instantiate(workoutProgressEntryPrefab, content).GetComponent<MainMenu_WorkoutHistoryEntry>();
            workoutHistoryEntry.setEntryData(PlayerDataManager.instance.getProgramDataById(workoutProgramsHistoryData[currentIndex].programId-1),workoutProgramsHistoryData[currentIndex]);
        }
    }
}
