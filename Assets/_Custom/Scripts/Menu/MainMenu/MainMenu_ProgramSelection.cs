﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu_ProgramSelection : MonoBehaviour
{
    public Transform content;
    public int siblingIndexForCurrentProgram;

    private bool flag_firstTime = true;
    private Transform currentProgram;

    void Start()
    {
        flag_firstTime = true;

        var currentProgramId = PlayerPrefs.GetInt(GlobalValue.selectedWorkoutProgramKey, 0);
        var allPrograms = content.GetComponentsInChildren<MainMenu_Program>();
        foreach (MainMenu_Program p in allPrograms)
        {
            if (currentProgramId == p.programId)
            {
                currentProgram = p.transform;
                onProgramSelect(currentProgram);
            }
        }
    }

    public void onProgramSelect(Transform selectedProgram)
    {
        if (currentProgram.Equals(selectedProgram))
        {
            selectedProgram.GetComponent<MainMenu_Program>().isCurrent = true;
            selectedProgram.SetSiblingIndex(siblingIndexForCurrentProgram);
        }
        else
        {
            currentProgram.GetComponent<MainMenu_Program>().isCurrent = false;
            selectedProgram.GetComponent<MainMenu_Program>().isCurrent = true;

            if (/*PlayerPrefs.GetInt("flag_firstTime", 0) == 0*/ flag_firstTime)
                currentProgram.SetSiblingIndex((currentProgram.GetComponent<MainMenu_Program>().programId) + 1);
            else
                currentProgram.SetSiblingIndex((currentProgram.GetComponent<MainMenu_Program>().programId) + 2);

            selectedProgram.SetSiblingIndex(siblingIndexForCurrentProgram);
        }

        if (!flag_firstTime)
            MainMenuManager.instance.btn_backFromProgramSelection();

        //PlayerPrefs.SetInt("flag_firstTime", 1);
        flag_firstTime = false;
        currentProgram = selectedProgram;

        StartCoroutine(scrollToTop());
    }

    private IEnumerator scrollToTop()
    {
        yield return new WaitForEndOfFrame();
        GetComponentInChildren<ScrollRect>().verticalNormalizedPosition = 1;

    }
}