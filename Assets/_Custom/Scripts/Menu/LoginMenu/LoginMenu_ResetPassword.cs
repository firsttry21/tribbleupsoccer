﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class LoginMenu_ResetPassword : MonoBehaviour
{
    private readonly string emailPattern = @"^\D\w{6,}@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"; // Email address pattern modified
    private readonly string strongPasswordPattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,32}$";

    #region Password Reset UI Components
    public GameObject emailInputScreen;
    public GameObject CodeInputScreen;
    public GameObject newPasswordInputScreen;
    public GameObject resetPasswordEndScreen;

    public InputField emailInput;
    public Text emailInputErrorText;
    public InputField codeInput;
    public Text codeInputErrorText;
    public InputField newPasswordInput;
    public InputField confirmPasswordInput;
    public Text confirmPasswordErrorText;
    #endregion

    private string resetRequestEmail;
    private string resetRequestCode;
    private string resetRequestPassword;

    private GameObject currentScreen;

    private void Start()
    {
        resetAllErrorText();
        startScreen(emailInputScreen);
    }

    #region password reset web response handlers

    private void onSendPasswordResetEmailSuccess()
    {
        LoginSignupManager.instance.showHideInterimLoading(false);
        resetRequestEmail = emailInput.text;
        startScreen(CodeInputScreen);
    }

    private void onSendPasswordResetEmailFail(string errMsg)
    {
        LoginSignupManager.instance.showHideInterimLoading(false);
        emailInputErrorText.text = errMsg;
    }

    private void onCodeMatchSuccess()
    {
        LoginSignupManager.instance.showHideInterimLoading(false);
        resetRequestCode = codeInput.text;
        startScreen(newPasswordInputScreen);
    }

    private void onCodeMatchFail(string errMsg)
    {
        LoginSignupManager.instance.showHideInterimLoading(false);
        codeInputErrorText.text = errMsg;
    }

    private void onResetPasswordSuccess()
    {
        LoginSignupManager.instance.showHideInterimLoading(false);
        resetRequestPassword = confirmPasswordInput.text;
        startScreen(resetPasswordEndScreen);
    }

    private void onResetPasswordFail(string errMsg)
    {
        confirmPasswordErrorText.text = errMsg;
    }

    #endregion

    #region btn events

    public void btn_requestResetPasswordEmail()
    {
        if (!Regex.IsMatch(emailInput.text, emailPattern))
        {
            onSendPasswordResetEmailFail("Please enter a valid email address.");
            return;
        }

        SoccerWebApi.instance.sendResetPasswordEmail(emailInput.text, onSendPasswordResetEmailSuccess, onSendPasswordResetEmailFail);
    }

    public void input_requestCodeMatch()
    {
        codeInput.text = codeInput.text.ToUpper();

        if (codeInput.text.Length == codeInput.characterLimit)
        {
            LoginSignupManager.instance.showHideInterimLoading(false);
            Debug.Log("Matching...");
            SoccerWebApi.instance.matchResetPasswordCode(resetRequestEmail, codeInput.text, onCodeMatchSuccess, onCodeMatchFail);
        }
    }

    public void btn_requestResetPassword()
    {
        if (newPasswordInput.text == string.Empty && confirmPasswordInput.text == string.Empty)
        {
            confirmPasswordErrorText.text = "Please choose a password.";
            return;
        }
        else if (!newPasswordInput.text.Equals(confirmPasswordInput.text))
        {
            confirmPasswordErrorText.text = "Passwords do not match.";
            return;
        }
        else if (!Regex.IsMatch(newPasswordInput.text, strongPasswordPattern) || !Regex.IsMatch(newPasswordInput.text, strongPasswordPattern))
        {
            confirmPasswordErrorText.text = "Password must be atleast 8 characters and contain a mix of capital and small letters as well as symbols.";
            return;
        }

        LoginSignupManager.instance.showHideInterimLoading(true);
        SoccerWebApi.instance.postResetPasswordRequest(resetRequestEmail, confirmPasswordInput.text, onResetPasswordSuccess, onResetPasswordFail);
    }

    public void btn_endResetPassword()
    {
        closeAllResetPasswordScreens();
        LoginSignupManager.instance.showUiPanel(LoginSignupManager.instance.loginPanel);
    }

    #endregion

    private void resetAllErrorText()
    {
        emailInputErrorText.text = string.Empty;
        codeInputErrorText.text = string.Empty;
        confirmPasswordErrorText.text = string.Empty;
    }

    private void closeAllResetPasswordScreens()
    {
        emailInputScreen.SetActive(false);
        CodeInputScreen.SetActive(false);
        newPasswordInputScreen.SetActive(false);
    }

    private void startScreen(GameObject screenToStart)
    {
        if (currentScreen != null)
        {
            screenToStart.SetActive(true);
            currentScreen.SetActive(false);
            currentScreen = screenToStart;
        }
        else
        {
            closeAllResetPasswordScreens();
            screenToStart.SetActive(true);
            currentScreen = screenToStart;
        }
    }

}
