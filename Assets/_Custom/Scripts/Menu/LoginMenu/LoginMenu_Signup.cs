﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginMenu_Signup : MonoBehaviour
{
    [Header("Signup UI Components")]
    public GameObject signupEmailInputScreen;
    public GameObject otpInputScreen;
    public GameObject DateNameScreen;
    public GameObject usernamePasswordInputScreen;

    [Header("Signup Input Fields")]
    public InputField signupEmailInput;
    public Text signupEmailErrorText;
    public InputField otpInput;
    public Text signupOtpErrorText;
    public DateDropdownGenerator dateInput;
    public Text dateInputErrorText;
    public InputField firstNameInput;
    public Text firstNameInputErrorText;
    public InputField lastNameInput;
    public Text lastNameInputErrorText;
    public Toggle hasAcceptedTC;
    public Text signupTcErrorText;
    public InputField signupUsernameInput;
    public Text signupUsernameInputErrorText;
    public InputField signupPasswordInput;
    public Text signupPasswordInputErrorText;

    public UserInfo userInfo { get; private set; }

    private GameObject currentScreen;

    private void Start()
    {
        resetAllErrors();
        startScreen(signupEmailInputScreen);
    }

    private void startScreen(GameObject screenToStart)
    {
        if (currentScreen != null)
        {
            screenToStart.SetActive(true);
            currentScreen.SetActive(false);
            currentScreen = screenToStart;
        }
        else
        {
            closeAllScreens();
            screenToStart.SetActive(true);
            currentScreen = screenToStart;
        }
    }

    private void closeAllScreens()
    {
        signupEmailInputScreen.SetActive(false);
        otpInputScreen.SetActive(false);
        DateNameScreen.SetActive(false);
        usernamePasswordInputScreen.SetActive(false);
    }

    public void onSignupSuccess(UserInfo signupUserInfo)
    {
        LoginSignupManager.instance.showHideInterimLoading(false);

        //show family members to select here
        PlayerPrefs.SetInt(GlobalValue.loginStatusKey, 1);
        PlayerPrefs.SetString(GlobalValue.currentPlayerUsernameKey, signupUserInfo.emailAddress);
        PlayerPrefs.SetString(GlobalValue.currentPlayerPasswordKey, signupUserInfo.password);

        PlayerDataManager.instance.setUserInfo(signupUserInfo);

        PlayerPrefs.DeleteKey(GlobalValue.mainmenuNavigationInfoKey);

        SceneManager.LoadScene("MainMenuScene");
    }

    public void onSignupFail(string errorMsg)
    {
        LoginSignupManager.instance.showHideInterimLoading(false);
        signupPasswordInputErrorText.text = errorMsg;
    }

    public void onSignupStepSuccess(SignupStep currentStep)
    {
        LoginSignupManager.instance.showHideInterimLoading(false);

        switch (currentStep)
        {
            case SignupStep.Email:
                userInfo = new UserInfo();
                userInfo.emailAddress = signupEmailInput.text;

                startScreen(otpInputScreen);
                break;

            case SignupStep.OTP:
                startScreen(DateNameScreen);
                break;

            case SignupStep.DateFullName:
                userInfo.dob = dateInput.getSelectedBirthday();
                userInfo.firstName = firstNameInput.text;
                userInfo.lastName = lastNameInput.text;

                startScreen(usernamePasswordInputScreen);
                break;

            case SignupStep.UsernamePassword:
                userInfo.username = signupUsernameInput.text;
                userInfo.password = signupPasswordInput.text;

                SoccerWebApi.instance.postSignupRequest(userInfo, onSignupSuccess, onSignupFail);
                break;

            default:
                break;
        }
    }

    public void onSignupStepFail(SignupStep currentStep, string errorMsg)
    {
        LoginSignupManager.instance.showHideInterimLoading(false);

        switch (currentStep)
        {
            case SignupStep.Email:
                signupEmailErrorText.text = errorMsg;
                break;

            case SignupStep.OTP:
                signupOtpErrorText.text = errorMsg;
                break;

            case SignupStep.DateFullName:
                break;

            case SignupStep.UsernamePassword:
                signupUsernameInputErrorText.text = errorMsg;
                break;

            default:
                break;
        }
    }

    public void btn_setSignupEmailIfAvailable()
    {
        if (!Regex.IsMatch(signupEmailInput.text, LoginSignupManager.emailPattern))
        {
            onSignupStepFail(SignupStep.Email, "Please enter a valid email address.");
            return;
        }

        Debug.Log("Checking...");
        LoginSignupManager.instance.showHideInterimLoading(true);
        SoccerWebApi.instance.isEmailOrUsernameAvailable(SignupStep.Email, signupEmailInput.text, onSignupStepSuccess, onSignupStepFail);
    }

    public void input_matchOtpInput()
    {
        otpInput.text = otpInput.text.ToUpper();

        if (otpInput.text.Length == otpInput.characterLimit)
        {
            //TODO (2) check for otp match
            LoginSignupManager.instance.showHideInterimLoading(true);
            Debug.Log("Matching...");
            SoccerWebApi.instance.checkForOtpMatch(SignupStep.OTP, userInfo.emailAddress, otpInput.text, onSignupStepSuccess, onSignupStepFail);
        }
    }

    public void btn_setDateName()
    {
        if (formHasError())
            return;

        onSignupStepSuccess(SignupStep.DateFullName);
    }

    public void btn_signupIfUsernamePasswordIsValid()
    {
        if (signupUsernamePasswordFormHasError()) return;

        LoginSignupManager.instance.showHideInterimLoading(true);
        SoccerWebApi.instance.isEmailOrUsernameAvailable(SignupStep.UsernamePassword, signupUsernameInput.text, onSignupStepSuccess, onSignupStepFail); ;
    }

    private bool signupUsernamePasswordFormHasError()
    {
        bool hasError = false;
        if (signupUsernameInput.text == string.Empty)
        {
            signupUsernameInputErrorText.text = "Please choose a username.";
            hasError = true;
        }
        if (!Regex.IsMatch(signupUsernameInput.text, LoginSignupManager.usernamePattern))
        {
            signupUsernameInputErrorText.text = "Please choose a valid username. Atleast 6 characters.";
            hasError = true;
        }
        if (signupPasswordInput.text == string.Empty)
        {
            signupPasswordInputErrorText.text = "Please choose a password.";
            hasError = true;
        }
        if (!Regex.IsMatch(signupPasswordInput.text, LoginSignupManager.strongPasswordPattern))
        {
            signupPasswordInputErrorText.text = "Password must be atleast 8 characters and contain a mix of capital and small letters as well as symbols.";
            hasError = true;
        }
        if (signupUsernameInput.text.Length < 6)
        {
            signupUsernameInputErrorText.text = "Username must be atleast 6 characters.";
            hasError = true;
        }
        return hasError;
    }

    private bool formHasError()
    {
        bool hasError = false;
        if (dateInput.getSelectedBirthday() == null)
        {
            dateInputErrorText.text = "Please select a valid birthday.";
            hasError = true;
        }
        if (firstNameInput.text == string.Empty)
        {
            firstNameInputErrorText.text = "Please enter in your first name.";
            hasError = true;
        }
        if (!Regex.IsMatch(firstNameInput.text, LoginSignupManager.namePattern))
        {
            firstNameInputErrorText.text = "Please enter a valid name.";
            hasError = true;
        }
        if (lastNameInput.text == string.Empty)
        {
            lastNameInputErrorText.text = "Please enter in your last name.";
            hasError = true;
        }
        if (!Regex.IsMatch(lastNameInput.text, LoginSignupManager.namePattern))
        {
            lastNameInputErrorText.text = "Please enter a valid name.";
            hasError = true;
        }
        if (!hasAcceptedTC.isOn)
        {
            signupTcErrorText.text = "Please accept terms and conditions to continue.";
            hasError = true;
        }
        return hasError;
    }

    public void resetAllErrors()
    {
        signupEmailErrorText.text = string.Empty;
        signupOtpErrorText.text = string.Empty;
        dateInputErrorText.text = string.Empty;
        firstNameInputErrorText.text = string.Empty;
        lastNameInputErrorText.text = string.Empty;
        signupTcErrorText.text = string.Empty;
        signupUsernameInputErrorText.text = string.Empty;
        signupPasswordInputErrorText.text = string.Empty;
    }
}

public enum SignupStep
{
    Email,
    OTP,
    DateFullName,
    UsernamePassword
}