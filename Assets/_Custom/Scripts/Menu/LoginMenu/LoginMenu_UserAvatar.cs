﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class LoginMenu_UserAvatar : MonoBehaviour
{
    public Text displayNameText;
    public GameObject activeUserIndicator;
    [HideInInspector] public int associatedFamilyId;
    [HideInInspector] public bool isCurrent;

    public void setAsActiveUser(bool isActive)
    {
        activeUserIndicator.SetActive(isActive);
    }

    public void btn_requestMemberData()
    {
        //MainMenuManager.instance.postRetrieveMemberDataRequest(associatedFamilyId);
        PlayerPrefs.SetInt(GlobalValue.selectedPlayerIdKey, associatedFamilyId);
        MainMenuManager.instance.postRetrieveMemberDataRequest(1);

        //MainMenuManager.instance.onRetrieveDataSucceed(File.ReadAllText(GlobalValue.saveFilePath));
    }
}
