﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginMenu_MembersScreen : MonoBehaviour
{
    public Text emailLabel;
    public Transform content;
    public GameObject userAvatarPrefab;
    public GameObject plusButton;

    private ScrollRect usersScrollView;

    private void Awake()
    {
        usersScrollView = GetComponentInChildren<ScrollRect>();
    }

    public void listAllMembers(int lastLoginFamilyId, List<FamilyMember> members)
    {
        emailLabel.text = "<b>Your Account:</b> " + PlayerDataManager.instance.userInfo.emailAddress;

        if (members.Count >= 5)
            plusButton.SetActive(false);

        foreach (Transform t in content.transform)
        {
            Destroy(t.gameObject);
        }

        foreach (var member in members)
        {
            LoginMenu_UserAvatar userAvatar = GameObject.Instantiate(userAvatarPrefab, content).GetComponent<LoginMenu_UserAvatar>();
            userAvatar.displayNameText.text = member.memberName;
            userAvatar.associatedFamilyId = member.familyId;
            if (lastLoginFamilyId == member.familyId)
                userAvatar.setAsActiveUser(true);
            else
                userAvatar.setAsActiveUser(false);
        }

        StartCoroutine(scrollToTopCo());
    }

    private IEnumerator scrollToTopCo()
    {
        yield return new WaitForEndOfFrame();
        content.GetComponentInParent<ScrollRect>().horizontalNormalizedPosition = 0;
    }
}
