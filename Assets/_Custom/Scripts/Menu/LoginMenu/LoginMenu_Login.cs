﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginMenu_Login : MonoBehaviour
{
    [Header("Login UI Components")]
    public GameObject loginCredentialsInputScreen;

    [Header("Login Input Fields")]
    public InputField loginEmailUsernameInput;
    public InputField loginPasswordInput;
    public Text loginErrorText;

    private GameObject currentScreen;

    private void Start()
    {
        loginErrorText.text = string.Empty;
    }

    private void startScreen(GameObject screenToStart)
    {
        if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            screenToStart.SetActive(true);
            currentScreen = screenToStart;
        }
        else
        {
            closeAllScreens();
            screenToStart.SetActive(true);
            currentScreen = screenToStart;
        }
    }

    private void closeAllScreens()
    {
        loginCredentialsInputScreen.SetActive(false);
    }

    public void onLoginSuccess(UserInfo userData)
    {
        //show family members to select here
        PlayerPrefs.SetInt(GlobalValue.loginStatusKey, 1);
        PlayerPrefs.SetString(GlobalValue.currentPlayerUsernameKey, loginEmailUsernameInput.text);
        PlayerPrefs.SetString(GlobalValue.currentPlayerPasswordKey, loginPasswordInput.text);

        PlayerDataManager.instance.setUserInfo(userData);

        PlayerPrefs.DeleteKey(GlobalValue.mainmenuNavigationInfoKey);

        SceneManager.LoadSceneAsync("MainMenuScene");
    }

    public void onLoginFail(string errorMsg)
    {
        LoginSignupManager.instance.showHideInterimLoading(false);
        //show error msg near password or something ¯\_(ツ)_/¯
        loginErrorText.text = errorMsg;
    }

    public void btn_sendLoginRequest()
    {
        //when user trynna be fishy
        if (loginEmailUsernameInput.text == string.Empty || loginPasswordInput.text == string.Empty)
        {
            onLoginFail("Email or Password fields cannot be empty.");
            return;
        }
        if (!Regex.IsMatch(loginEmailUsernameInput.text, LoginSignupManager.emailPattern) &&
            !Regex.IsMatch(loginEmailUsernameInput.text, LoginSignupManager.usernamePattern))
        {
            onLoginFail("Please input valid Email or Username.");
            return;
        }

        //send login input fields data to (Login) function
        LoginSignupManager.instance.showHideInterimLoading(true);
        SoccerWebApi.instance.postLoginRequest(loginEmailUsernameInput.text, loginPasswordInput.text, onLoginSuccess, onLoginFail);
    }
}
