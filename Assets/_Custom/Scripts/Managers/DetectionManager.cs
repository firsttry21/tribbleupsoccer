﻿using OpenCvSharp;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DetectionManager : MonoBehaviour
{
    public static DetectionManager instance;

    public RectTransform scanningModeDetectionCircle;
    public RectTransform inGameDetectionCircle;
    public RectTransform juggleModeDetectionCircle;
    public GameObject jugglesKillZone;

    private DetectionCanvasScript detectionCanvas;
    private UiDetector[] uiDetectors;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        detectionCanvas = GetComponent<DetectionCanvasScript>();
        uiDetectors = GetComponentsInChildren<UiDetector>();
    }

    private void Start()
    {
        showHideDetectionCircle(false);
        jugglesKillZone.SetActive(false);
    }

    public void drawDetectionCircle(Size imageSize, Vector2 center, float radius)
    {
        detectionCanvas.drawUiCircle(imageSize, center, radius);
    }

    public void setToScanMode(bool isScanning = true, GameManager.GameMode gameMode = GameManager.GameMode.Workout)
    {
        if (isScanning)
        {
            detectionCanvas.detectorRect = scanningModeDetectionCircle;
            inGameDetectionCircle.gameObject.SetActive(false);
            juggleModeDetectionCircle.gameObject.SetActive(false);
        }
        else
        {
            detectionCanvas.detectorRect = gameMode == GameManager.GameMode.Workout ||
                                           gameMode == GameManager.GameMode.Practice ?
                                           inGameDetectionCircle :
                                           juggleModeDetectionCircle;
            scanningModeDetectionCircle.gameObject.SetActive(false);
        }
    }

    public void showHideDetectionCircle(bool status)
    {
        detectionCanvas.setDetectorRectActive(status);
    }

    public void showHideUiDetectors(bool status)
    {
        foreach (UiDetector ui in uiDetectors)
        {
            ui.gameObject.SetActive(status);
        }

        detectionCanvas.detectorWhereBallWasDetectedLastTime = uiDetectors[uiDetectors.Length - 1].gameObject;
    }

    public void startJugglingMode(bool status)
    {
        jugglesKillZone.SetActive(status);
    }

    public void setPositionsForBallDetection(float pos)
    {
        detectionCanvas.setDetectionUi(pos);
    }
}