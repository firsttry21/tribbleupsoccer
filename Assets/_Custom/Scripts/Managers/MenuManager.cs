﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;

    public GameObject camCanvas;
    public GameObject ballScanningScreen;
    public GameObject drillsLeftScreen;
    public GameObject GUI;
    public GameObject practiceGUI;
    public GameObject jugglingGUI;
    public GameObject pauseScreen;
    public GameObject resultScreen;

    private Menu_resultScreen resultScreenScript;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        ballScanningScreen.SetActive(false);
        drillsLeftScreen.SetActive(false);
        GUI.SetActive(false);
        pauseScreen.SetActive(false);
        resultScreen.SetActive(false);
    }

    private void Start()
    {
        startBallScanning();
        resultScreenScript = GetComponentInChildren<Menu_resultScreen>(true);
    }

    public void startBallScanning()
    {
        camCanvas.SetActive(true);
        DetectionManager.instance.setToScanMode();
        DetectionManager.instance.showHideUiDetectors(false);
        ballScanningScreen.SetActive(true);
    }

    public void startPracticeDrill()
    {
        camCanvas.SetActive(true);
        DetectionManager.instance.showHideDetectionCircle(false);
        DetectionManager.instance.showHideUiDetectors(false);
        ballScanningScreen.SetActive(false);
        practiceGUI.SetActive(false);
        drillsLeftScreen.SetActive(true);

        StartCoroutine(startPracticeAfterCo(2.5f));
    }

    IEnumerator startPracticeAfterCo(float dur)
    {
        yield return new WaitForSeconds(dur);

        drillsLeftScreen.SetActive(false);
        waitforDrillVideo();
        practiceGUI.SetActive(true);
        DetectionManager.instance.setToScanMode(false, GameManager.GameMode.Practice);
        DetectionManager.instance.showHideDetectionCircle(true);
        DetectionManager.instance.showHideUiDetectors(true);

        GameManager.instance.startPracticeGame();
    }

    public void startNextWorkoutDrill()
    {
        camCanvas.SetActive(true);
        DetectionManager.instance.showHideDetectionCircle(false);
        DetectionManager.instance.showHideUiDetectors(false);
        ballScanningScreen.SetActive(false);
        GUI.SetActive(false);
        drillsLeftScreen.SetActive(true);

        StartCoroutine(startWorkoutAfterCo(2.5f));
    }

    internal void endWorkout(DTO_WorkoutProgram program)
    {
        camCanvas.SetActive(false);
        DetectionManager.instance.showHideDetectionCircle(false);
        DetectionManager.instance.showHideUiDetectors(false);
        pauseScreen.SetActive(false);
        GUI.SetActive(false);
        practiceGUI.SetActive(false);
        jugglingGUI.SetActive(false);
        showResultScreen(program);
    }

    internal void showResultScreen(DTO_WorkoutProgram program)
    {
        resultScreen.SetActive(true);
        resultScreenScript.showResultFor(program);
    }

    IEnumerator startWorkoutAfterCo(float dur)
    {
        yield return new WaitForSeconds(dur);

        drillsLeftScreen.SetActive(false);
        waitforDrillVideo();
        GUI.SetActive(true);
        DetectionManager.instance.setToScanMode(false);
        DetectionManager.instance.showHideDetectionCircle(true);
        DetectionManager.instance.showHideUiDetectors(true);

        GameManager.instance.startGame();
    }

    public void startJugglingMode()
    {
        camCanvas.SetActive(true);
        ballScanningScreen.SetActive(false);
        DetectionManager.instance.setToScanMode(false, GameManager.GameMode.Juggling);
        DetectionManager.instance.startJugglingMode(true);
        jugglingGUI.SetActive(true);
        DetectionManager.instance.showHideDetectionCircle(true);

        GameManager.instance.startJuggleModeGame();
    }

    private void waitforDrillVideo()
    {

    }

    public void showHideDetectionWarning(bool shouldShowWarning, bool isTooFar = false)
    {
        //warningText.text = isTooFar ? "MOVE CLOSER" : "MOVE FARTHER";
        //warningPanel.SetActive(shouldShowWarning);
    }

    public void btn_pause()
    {
        if (GameManager.instance.state == GameManager.GameState.Playing)
        {
            pauseScreen.SetActive(true);
            DetectionManager.instance.showHideDetectionCircle(false);
            DetectionManager.instance.showHideUiDetectors(false);
            //GUI.SetActive(false);
            GameManager.instance.pauseGame(true);
            Time.timeScale = 0;
        }
        else if (GameManager.instance.state == GameManager.GameState.Paused)
        {
            Time.timeScale = 1;
            GameManager.instance.pauseGame(false);
            DetectionManager.instance.showHideUiDetectors(true);
            DetectionManager.instance.showHideDetectionCircle(true);
            //GUI.SetActive(true);
            pauseScreen.SetActive(false);
        }
        else
            return;
    }

    public void btn_replayDrill()
    {
        pauseScreen.SetActive(false);
        if (GameManager.instance.gameMode == GameManager.GameMode.Workout) GameManager.instance.startWorkoutDrill();
        else GameManager.instance.startPracticeDrill();
    }

    public void btn_nextDrill()
    {
        pauseScreen.SetActive(false);
        LevelManager.instance.onDrillTimerDone();
    }

    public void btn_quitWorkout()
    {
        camCanvas.SetActive(false);
        StartCoroutine(waitForQuitCo(1f));
    }

    public void btn_doneWorkout()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void btn_endWorkout()
    {
        pauseScreen.SetActive(false);
        GameManager.instance.endWorkout();
    }
    public void btn_endPractice()
    {
        pauseScreen.SetActive(false);
        //GameManager.instance.endWorkout();
    }
    public void btn_endJuggling()
    {
        pauseScreen.SetActive(false);
        //GameManager.instance.endWorkout();
    }

    IEnumerator waitForQuitCo(float dur)
    {
        yield return new WaitForSecondsRealtime(dur);

        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenuScene");
    }

    public void btn_repeatWorkout()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("WorkoutScene");
    }
}