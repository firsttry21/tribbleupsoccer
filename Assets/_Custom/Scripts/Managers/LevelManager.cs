﻿using System;
using System.Collections;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public DTO_WorkoutProgram selectedProgram;
    [HideInInspector] public WorkoutDrill selectedDrill;
    [HideInInspector] public WorkoutDrill[] drillsForSelectedProgram;
    public int currentDrillIndex { get; private set; }
    public float currentTimer { get; set; }

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public DTO_WorkoutProgram initializeWorkoutMode(int selectedProgramId)
    {
        if (WorkoutProgramHolder.instance != null) //TODO remove in final build
        {
            selectedProgram = WorkoutProgramHolder.instance.getSelectedProgram(selectedProgramId);
            drillsForSelectedProgram = selectedProgram.workoutDrills;
        }
        else//TODO remove in final build
        {
            //this is only for edge case when workout scene is run directly from editor and WorkoutProgramHolder is not in scene,
            //so we pick default program assigned in inspector
            drillsForSelectedProgram = selectedProgram.workoutDrills;
        }
        currentDrillIndex = 0;

        return selectedProgram;
    }

    public void initializePracticeMode()
    {
        if (WorkoutProgramHolder.instance != null)
        {
            selectedDrill = WorkoutProgramHolder.instance.getSelectedDrill();
        }
    }

    public void initializeJuggleMode()
    {

    }

    public void startDrill()
    {
        DetectionManager.instance.setPositionsForBallDetection(drillsForSelectedProgram[currentDrillIndex].detectorPosX);
        currentTimer = drillsForSelectedProgram[currentDrillIndex].totalTime;
        StartCoroutine(CountDownTimer());
    }

    public void startPracticeDrill()
    {
        DetectionManager.instance.setPositionsForBallDetection(selectedDrill.detectorPosX);
    }

    public void onDrillTimerDone()
    {
        currentDrillIndex++;
        GameManager.instance.startWorkoutDrill();
    }

    IEnumerator CountDownTimer()
    {
        yield return new WaitForSeconds(1);

        if (GameManager.instance.state != GameManager.GameState.Playing)
            yield break;

        currentTimer--;
        GameManager.instance.addWorkoutDrillTimeScore(currentDrillIndex, 1);

        if (currentTimer <= 0)
        {
            if (currentDrillIndex < (drillsForSelectedProgram.Length - 1))
            {
                onDrillTimerDone();
            }
            else
            {
                GameManager.instance.endWorkout();
            }
        }

        if (currentTimer > 0)
            StartCoroutine(CountDownTimer());
    }

    public string getDrillsLeftMessage()
    {
        if (GameManager.instance.gameMode == GameManager.GameMode.Practice)
            return "PRACTICE";

        return currentDrillIndex == drillsForSelectedProgram.Length - 1 ?
            "LAST DRILL" :
            (drillsForSelectedProgram.Length - currentDrillIndex).ToString() + " DRILLS TO GO";
    }
}