﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;

public class WorkoutManager : MonoBehaviour
{
    public static WorkoutManager instance;

    public List<WorkoutParameters> workoutLevels;

    public bool isDetectionPaused { get; set; }
    public bool isGameOver { get; set; }

    private int selectedWorkout;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        setSelectedWorkout(selectedWorkout);
    }

    public void changeSelectionWithButton()
    {
        string clickedButtonNumber = EventSystem.current.currentSelectedGameObject.name;
        Debug.Log("selection=" + clickedButtonNumber);
        bool isInt; int workoutToSelect;
        isInt = int.TryParse(clickedButtonNumber, out workoutToSelect);
        if (isInt)
            setSelectedWorkout(workoutToSelect);
    }

    public void setSelectedWorkout(int w)
    {
        if (workoutLevels.Count <= 0) return;

        selectedWorkout = w;
        WorkoutParameters selectedWorkoutParams = new WorkoutParameters(workoutLevels[selectedWorkout].reps, workoutLevels[selectedWorkout].time,
                                                                        workoutLevels[selectedWorkout].drills, workoutLevels[selectedWorkout].detectorPosX);

        StatsManager.instance.setSelectedWorkoutParams(selectedWorkoutParams);
        //DetectionManager.instance.setPositionsForBallDetection(selectedWorkoutParams);

    }
}
