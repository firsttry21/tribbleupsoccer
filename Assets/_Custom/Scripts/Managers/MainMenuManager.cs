﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager instance;

    public Text memberDataRetrieveErrorText;
    public GameObject memberSelectScreen;
    public GameObject createMemberScreen;
    public GameObject resultScreen;
    public GameObject settingScreen;
    public GameObject juggleScreen;
    public GameObject practiceScreen;
    public GameObject lbScreen;
    public GameObject progressScreen;
    public GameObject programSelectionScreen;
    public GameObject programDetailsScreen;
    public GameObject sideMenuPanel;
    public GameObject loadingScreen;

    private LoginMenu_MembersScreen memberSelectScript;
    private Menu_resultScreen resultScreenScript;
    private int currentlyViewingResultProgramId;

    private GameObject currentScreen;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        resultScreenScript = GetComponentInChildren<Menu_resultScreen>(true);
        memberSelectScript = memberSelectScreen.GetComponentInChildren<LoginMenu_MembersScreen>(true);
    }

    private void Start()
    {
        hideAllScreens();
        showHideLoadingScreen(false);

        string navigationInfo = PlayerPrefs.GetString(GlobalValue.mainmenuNavigationInfoKey, memberSelectScreen.name);
        if (navigationInfo == memberSelectScreen.name)
        {
            startUserSelectScreen(PlayerDataManager.instance.userInfo.membersList);
            memberDataRetrieveErrorText.text = string.Empty;
        }
        else if (navigationInfo == programDetailsScreen.name)
        {
            startScreen(programDetailsScreen);
        }
        else if (navigationInfo == progressScreen.name)
        {
            startScreen(progressScreen);
        }
        else if (navigationInfo == lbScreen.name)
        {
            startScreen(lbScreen);
        }
        else if (navigationInfo == practiceScreen.name)
        {
            startScreen(practiceScreen);
        }
        else if (navigationInfo == juggleScreen.name)
        {
            startScreen(juggleScreen);
        }
    }

    public void startResultScreenForProgram(DTO_WorkoutProgram program)
    {
        currentlyViewingResultProgramId = program.programId;
        resultScreen.SetActive(true);
        resultScreenScript.showResultFor(program);
    }

    public void postRetrieveMemberDataRequest(int memberId)
    {
        showHideLoadingScreen(true);
        SoccerWebApi.instance.postRetrieveDataRequest(memberId);
    }

    public void onRetrieveDataSucceed(DTO_PlayerData memberData)
    {
        showHideLoadingScreen(false);
        PlayerDataManager.instance.setPlayerData(memberData);
        startScreen(programDetailsScreen);
    }

    public void onRetrieveDataFail(string errMsg)
    {
        showHideLoadingScreen(false);
        memberDataRetrieveErrorText.text = errMsg;
        PlayerPrefs.DeleteKey(GlobalValue.selectedPlayerIdKey);
    }

    public void startMemberSelectionScreen()
    {
        startScreen(memberSelectScreen);
    }

    #region Menu Buttons Actions

    public void btn_signoutPlayer()
    {
        PlayerDataManager.instance.onPlayerSignout();
    }

    public void btn_startProgramDetailScreen()
    {
        startScreen(programDetailsScreen);
    }

    public void btn_startProgressScreen()
    {
        startScreen(progressScreen);
    }

    public void btn_startPracticeScreen()
    {
        startScreen(practiceScreen);
    }

    public void btn_startJuggleScreen()
    {
        startScreen(juggleScreen);
    }

    public void btn_startLbScreen()
    {
        startScreen(lbScreen);
    }

    public void btn_startSettingScreen()
    {
        startScreen(settingScreen);
    }

    public void btn_backFromProgramSelection()
    {
        programDetailsScreen.SetActive(true);
        programSelectionScreen.GetComponent<DG.Tweening.DOTweenAnimation>().DOPlayForward();
    }

    public void btn_startInWorkoutMode()
    {
        PlayerPrefs.SetInt(GlobalValue.selectedGameModeKey, GlobalValue.gameModeWorkoutId);
        SceneManager.LoadScene("WorkoutScene");
    }

    public void btn_startInPracticeMode()
    {
        PlayerPrefs.SetInt(GlobalValue.selectedGameModeKey, GlobalValue.gameModePracticeId);
        SceneManager.LoadScene("Workout");
    }

    public void btn_startInJuggleMode()
    {
        PlayerPrefs.SetInt(GlobalValue.selectedGameModeKey, GlobalValue.gameModeJuggleId);
        SceneManager.LoadScene("Workout");
    }

    public void btn_repeatWorkoutFromProgress()
    {
        WorkoutProgramHolder.instance.setSelectedProgram(currentlyViewingResultProgramId);
        PlayerPrefs.SetInt(GlobalValue.selectedGameModeKey, GlobalValue.gameModeWorkoutId);
        SceneManager.LoadScene("Workout");
    }

    #endregion

    public void showHideLoadingScreen(bool isShow)
    {
        loadingScreen.SetActive(isShow);
    }

    private void startUserSelectScreen(List<FamilyMember> members)
    {
        memberSelectScreen.SetActive(true);
        int lastSelectedUserId = PlayerPrefs.GetInt(GlobalValue.selectedPlayerIdKey, 0);
        //if lastSelectedUser is 0 meaning non existent, then just set the first member as selected.
        if (lastSelectedUserId == 0) lastSelectedUserId = members[0].familyId;
        memberSelectScript.listAllMembers(lastSelectedUserId, members);
    }

    private void startScreen(GameObject screenToStart)
    {
        if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            screenToStart.SetActive(true);
            currentScreen = screenToStart;
            PlayerPrefs.SetString(GlobalValue.mainmenuNavigationInfoKey, currentScreen.name);
        }
        else
        {
            hideAllScreens();

            screenToStart.SetActive(true);
            currentScreen = screenToStart;
            PlayerPrefs.SetString(GlobalValue.mainmenuNavigationInfoKey, currentScreen.name);
        }

        sideMenuPanel.SetActive(false);
    }

    private void hideAllScreens()
    {
        memberSelectScreen.SetActive(false);
        resultScreen.SetActive(false);
        settingScreen.SetActive(false);
        juggleScreen.SetActive(false);
        practiceScreen.SetActive(false);
        lbScreen.SetActive(false);
        progressScreen.SetActive(false);
        programSelectionScreen.SetActive(false);
        programDetailsScreen.SetActive(false);
        sideMenuPanel.SetActive(false);
    }
}