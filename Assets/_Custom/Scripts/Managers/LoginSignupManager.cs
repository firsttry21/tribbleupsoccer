﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginSignupManager : MonoBehaviour
{
    public static LoginSignupManager instance;

    #region Regular Expressions
    //private readonly string emailPattern        = @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"; // Email address pattern original
    //private readonly string usernamePattern     = "^[a-z0-9_-]{6,16}$"; // username pattern
    //private readonly string mediumPasswordPattern = @"^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})";

    public static readonly string emailPattern = @"^\D\w{6,}@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"; // Email address pattern modified
    public static string usernamePattern = "^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"; // username pattern
    public static readonly string namePattern = @"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$"; //real name pattern
    public static readonly string strongPasswordPattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,32}$";
    #endregion

    #region UI Components
    public GameObject welcomeScreen;
    public GameObject loginPanel;
    public GameObject signupPanel;
    public GameObject resetPasswordPanel;

    [Header("Other")]
    public GameObject interimLoading;
    public GameObject LoadingAfterPlayerSelect;
    #endregion

    private GameObject currentPanel;
    private LoginMenu_Login loginController;
    private LoginMenu_Signup signupController;
    private LoginMenu_ResetPassword resetPasswordController;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(this.gameObject);
            return;
        }

        loginController = loginPanel.GetComponent<LoginMenu_Login>();
        signupController = signupPanel.GetComponent<LoginMenu_Signup>();
        resetPasswordController = resetPasswordPanel.GetComponent<LoginMenu_ResetPassword>();
    }

    private void Start()
    {
        if (PlayerPrefs.GetInt(GlobalValue.loginStatusKey) == 1)
        {
            showUiPanel(loginPanel);
            showHideInterimLoading(true);
            SoccerWebApi.instance.postLoginRequest(
               PlayerPrefs.GetString(GlobalValue.currentPlayerUsernameKey),
               PlayerPrefs.GetString(GlobalValue.currentPlayerPasswordKey),
               loginController.onLoginSuccess,
               loginController.onLoginFail);
            return;
        }
        else
        {
            showUiPanel(welcomeScreen);
        }
    }

    public void showUiPanel(GameObject panelToShow)
    {
        if (currentPanel != null)
        {
            panelToShow.SetActive(true);
            currentPanel.SetActive(false);
            currentPanel = panelToShow;
        }
        else
        {
            closeAllPanels();
            panelToShow.SetActive(true);
            currentPanel = panelToShow;
        }
    }

    private void closeAllPanels()
    {
        loginPanel.SetActive(false);
        signupPanel.SetActive(false);
        resetPasswordPanel.SetActive(false);
    }

    public void showHideInterimLoading(bool status)
    {
        interimLoading.SetActive(status);
    }

    public void btn_startLoginPanel()
    {
        showUiPanel(loginPanel);
    }

    public void btn_startSignupPanel()
    {
        showUiPanel(signupPanel);
    }

    public void btn_startResetPasswordPanel()
    {
        showUiPanel(resetPasswordPanel);
    }
}


[DataContract]
[Serializable]
public class FamilyMemberDataRequest
{
    [IgnoreDataMember] public string loginTokenId { get; private set; }
    [IgnoreDataMember] public string memberUsername { get; private set; }
    [DataMember(Name = "memberData")] public string memberDataJson;

    public FamilyMemberDataRequest(string token, string memberName)
    {
        loginTokenId = token; memberUsername = memberName;
    }
}