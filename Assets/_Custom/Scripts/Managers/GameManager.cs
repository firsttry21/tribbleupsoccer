﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public enum GameState { Starting, Playing, Paused, Finished, DetectionPaused }
    public GameState state { get; set; }
    public enum GameMode { Workout, Practice, Juggling }
    public GameMode gameMode = GameMode.Workout;

    public int currentDrillCompletedReps { get; set; }
    public int gainedPracticeScore { get; set; }
    public int gainedJuggleScore { get; set; }

    DTO_WorkoutProgram program;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        state = GameState.Starting;
    }

    private void Start()
    {
        switch (PlayerPrefs.GetInt(GlobalValue.selectedGameModeKey, 0))
        {
            case GlobalValue.gameModeWorkoutId:
                gameMode = GameMode.Workout;
                program = LevelManager.instance.initializeWorkoutMode(PlayerPrefs.GetInt(GlobalValue.selectedWorkoutProgramKey, 0));
                Debug.Log("program=" + program);
                break;
            case GlobalValue.gameModePracticeId:
                gameMode = GameMode.Practice;
                LevelManager.instance.initializePracticeMode();
                break;
            case GlobalValue.gameModeJuggleId:
                gameMode = GameMode.Juggling;
                LevelManager.instance.initializeJuggleMode();
                break;
            default:
                Debug.LogError("WTF");
                break;
        }
    }

    public void addWorkoutScore(int scoreToAdd)
    {
        program.workoutDrills[LevelManager.instance.currentDrillIndex].repsCompleted += scoreToAdd;
        if (program.workoutDrills[LevelManager.instance.currentDrillIndex].repsCompleted >= program.workoutDrills[LevelManager.instance.currentDrillIndex].personalBest)
            program.workoutDrills[LevelManager.instance.currentDrillIndex].personalBest = program.workoutDrills[LevelManager.instance.currentDrillIndex].repsCompleted;
        currentDrillCompletedReps += scoreToAdd;
    }

    public void addWorkoutDrillTimeScore(int drillIndex, int timeScoreToAdd)
    {
        program.workoutDrills[drillIndex].timeCompleted += timeScoreToAdd;
    }

    public void addPracticeScore(int score)
    {
        gainedPracticeScore += score;
    }

    public void addJuggleScore(int score)
    {
        gainedJuggleScore += score;
    }

    public void startWorkoutDrill()
    {
        if (program == null) return;

        program.totalCompletedReps += currentDrillCompletedReps;
        //save drill scores/times and shit
        currentDrillCompletedReps = 0;
        Time.timeScale = 1;
        state = GameState.Starting;
        MenuManager.instance.startNextWorkoutDrill();
    }

    public void startPracticeDrill()
    {//save drill scores/times and shit
        gainedPracticeScore = 0;
        Time.timeScale = 1;
        state = GameState.Starting;
        MenuManager.instance.startPracticeDrill();
    }

    public void startJuggleMode()
    {//save drill scores/times and shit
        Time.timeScale = 1;
        state = GameState.Starting;

        MenuManager.instance.startJugglingMode();
    }

    public void startGame()
    {
        state = GameState.Playing;
        LevelManager.instance.startDrill();
    }

    public void startPracticeGame()
    {
        state = GameState.Playing;
        LevelManager.instance.startPracticeDrill();
    }

    public void startJuggleModeGame()
    {
        state = GameState.Playing;
    }

    public void pauseGame(bool pause)
    {
        state = pause ? GameState.Paused : GameState.Playing;
    }

    public void endWorkout()
    {
        state = GameState.Finished;

        saveWorkoutProgramData(program);

        MenuManager.instance.endWorkout(program);
    }

    private void saveWorkoutProgramData(DTO_WorkoutProgram program)
    {
        program.totalCompletedReps += currentDrillCompletedReps;
        program.dateTimeWhenCompleted = DateTime.Now;
        //program.dateWhenCompleted = DateTime.Now.ToString("dddd MMMM dd");
        //program.timeWhenCompleted = DateTime.Now.ToString("hh:mm tt").ToUpper(); //eg: Tuesday March 3 04:27 pm
        program.daysCompleted += 1;
        if (PlayerDataManager.instance != null)
        {
            PlayerDataManager.instance.saveCompletedProgram(program);
        }
    }
}