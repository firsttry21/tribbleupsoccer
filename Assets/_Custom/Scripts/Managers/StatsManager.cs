﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{
    public static StatsManager instance;

    public WorkoutParameters selectedWorkoutParams { get; private set; }

    [HideInInspector]
    public bool isFirstTimeDetection = true;

    public Text totalScoreText;
    public Text currentScoreText;
    [Space]
    public Text timerText;
    public float levelTimer;

    private bool _isDetectionPaused = false;
    private int currentScore;
    private int totalScore;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
    }

    private void Start()
    {
        Debug.Assert(currentScoreText || totalScoreText, "Please assign all UI elements");

        isFirstTimeDetection = true;
        restartCurrentDrill();
    }

    private void Update()
    {
        if (WorkoutManager.instance.isGameOver) return;

        if (levelTimer > 0)
        {
            updateTimer();
        }
        else
        {
            WorkoutManager.instance.isGameOver = true;
            EventManager.TriggerEvent("loseCurrentDrill");
            EventManager.TriggerEvent("endGame");
            return;
        }
    }

    private void updateTimer()
    {
        levelTimer -= Time.deltaTime;
        timerText.text = ":" + levelTimer.ToString("00");
    }

    public void setTotalScore(int score)
    {
        totalScore = score;
        totalScoreText.text = "/" + score.ToString("00");
    }

    public void addScore(int amount)
    {
        if (WorkoutManager.instance.isDetectionPaused) return;

        if (currentScore < totalScore)
        {
            currentScore += amount;
            updateScoreUi();
            if (currentScore == totalScore)
            {
                WorkoutManager.instance.isGameOver = true;
                EventManager.TriggerEvent("winCurrentDrill");
                EventManager.TriggerEvent("endGame");
            }
        }
        else return;
    }

    public void setSelectedWorkoutParams(WorkoutParameters woParams, bool shouldAlsoReset = true)
    {
        this.selectedWorkoutParams = woParams;
        if (shouldAlsoReset)
            resetDrill(woParams);
    }

    private void restartCurrentDrill()
    {
        WorkoutManager.instance.isGameOver = false;

        if (selectedWorkoutParams != null)
        {
            resetDrill(selectedWorkoutParams);
        }
        updateScoreUi();
    }

    private void resetDrill(WorkoutParameters drillParams)
    {
        Debug.Log("DRILL RESETTTTTTTTT");
        currentScore = 0;
        totalScore = drillParams.reps;
        levelTimer = drillParams.time;

        setTotalScore(totalScore);
        updateScoreUi();
    }

    private void updateScoreUi()
    {
        currentScoreText.text = currentScore.ToString("00");
    }

    private void pauseDetection()
    {
        Debug.Log("detection paused by = " + this.name);
        this._isDetectionPaused = true;
    }

    private void unpauseDetection()
    {
        this._isDetectionPaused = false;
    }

    //public void setDetectionPaused(bool isPaused)
    //{
    //    this._isDetectionPaused = isPaused;
    //}
}
