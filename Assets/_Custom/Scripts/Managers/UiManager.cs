﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static UiManager instance;

    public GameObject warningPanel;
    public Text warningText;
    [Space]
    public GameObject drillSelectionPanel;
    public GameObject drillEndPanel;

    private bool isGameOver;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void OnEnable()
    {
        //EventManager.StartListening("showWarningForTooClose", showWarningForTooClose);
        //EventManager.StartListening("showWarningForTooFar", showWarningForTooFar);
        //EventManager.StartListening("hideWarning", hideWarning);
        //EventManager.StartListening("winCurrentDrill", winCurrentDrill);
        //EventManager.StartListening("loseCurrentDrill", loseCurrentDrill);
        EventManager.StartListening("restartCurrentDrill", restartGame);
        EventManager.StartListening("changeSelectionWithButton", onWorkoutSelectionChanged);
    }
    private void OnDisable()
    {
        //EventManager.StopListening("showWarningForTooClose", showWarningForTooClose);
        //EventManager.StopListening("showWarningForTooFar", showWarningForTooFar);
        //EventManager.StopListening("hideWarning", hideWarning);
        //EventManager.StopListening("winCurrentDrill", winCurrentDrill);
        //EventManager.StopListening("loseCurrentDrill", loseCurrentDrill);
        EventManager.StopListening("restartCurrentDrill", restartGame);
        EventManager.StopListening("changeSelectionWithButton", onWorkoutSelectionChanged);
    }

    private void Start()
    {
        restartGame();
        Debug.Assert(warningPanel || warningText || drillEndPanel, "ui element not assigned. Assign all respective ui elements to UiManager.");
    }

    private void restartGame()
    {
        showHideDetectionWarning(false);
        WorkoutManager.instance.isGameOver = false;
        drillEndPanel.SetActive(false);
    }

    public void showHideDetectionWarning(bool shouldShowWarning, bool isTooFar = false)
    {
        warningText.text = isTooFar ? "MOVE CLOSER" : "MOVE FARTHER";
        warningPanel.SetActive(shouldShowWarning);
    }

    private void showWarningForTooClose()
    {
        if (WorkoutManager.instance.isGameOver) return;

        warningText.text = "MOVE FARTHER";
        warningPanel.SetActive(true);
    }

    private void showWarningForTooFar()
    {
        if (WorkoutManager.instance.isGameOver) return;

        warningText.text = "MOVE CLOSER";
        warningPanel.SetActive(true);
    }

    private void hideWarning()
    {
        warningPanel.SetActive(false);
    }

    private void onWorkoutSelectionChanged()
    {
        drillSelectionPanel.SetActive(false);
    }

    private void winCurrentDrill()
    {
        print("drill winn...");
        WorkoutManager.instance.isGameOver = true;
        showHideDetectionWarning(false);

        drillEndPanel.SetActive(true);

    }

    private void loseCurrentDrill()
    {
        print("drill LOSE...");
        WorkoutManager.instance.isGameOver = true;
        showHideDetectionWarning(false);

        drillEndPanel.SetActive(true);
    }
}
