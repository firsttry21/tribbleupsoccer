﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public float speed = .5f;
    public float angle = 30f;
    float timer;

    void Start()
    {
        transform.Rotate(0, 0, 0);
        timer = 0;
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= speed)
        {
            transform.Rotate(0, 0, transform.rotation.z - angle);
            timer = 0;
        }
    }
}
