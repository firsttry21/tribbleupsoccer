﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicOperationData<T>
{
    public int status;
    public string message;
    public T data;
}
