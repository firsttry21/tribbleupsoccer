﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
[DataContract]
public class WorkoutDrill
{
    [DataMember(Name = "drillId")] public int drillId;
    public string drillName;
    public int totalReps;
    public int totalTime;
    [Range(0, 0.5f)] [DataMember(Name = "detectorXPos")] public float detectorPosX;
    [HideInInspector] public string[] practiceTags;

    [HideInInspector] [DataMember(Name = "repsCompleted")] public int repsCompleted;
    [HideInInspector] [DataMember(Name = "timeCompleted")] public int timeCompleted;
    [HideInInspector] [DataMember(Name = "personlBest")] public int personalBest;
}