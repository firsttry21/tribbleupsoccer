﻿using UnityEngine;

public struct GlobalValue
{
    public static readonly string            loginStatusKey = "loginStatus";
    public static readonly string             playerDataKey = "playerData";
    public static readonly string       selectedPlayerIdKey = "selectedPlayerId";
    public static readonly string mainmenuNavigationInfoKey = "mainmenuCurrentScreen";
    public static readonly string selectedWorkoutProgramKey = "currentWorkoutProgram";
    public static readonly string   selectedPracticeDrillId = "selectedPracticeDrillId";
    public static readonly string       selectedGameModeKey = "selectedGameMode";
    public static readonly string  completedProgramsDataKey = "completedProgramsData";
    public static readonly string        programsHistoryKey = "programsHistoryData";
    public static readonly string  currentPlayerUsernameKey = "currentPlayerUsername";
    public static readonly string  currentPlayerPasswordKey = "currentPlayerPassword";
    public static readonly string             maxJugglesKey = "jugglesHighScore";
    public static readonly string        uploadCacheDataKey = "uploadCacheData";

    public const int  gameModeWorkoutId = 0;
    public const int gameModePracticeId = 1;
    public const int   gameModeJuggleId = 2;

}