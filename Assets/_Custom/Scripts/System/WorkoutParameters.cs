﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WorkoutParameters
{
    public int reps;
    public float time;
    public int drills;
    [Range(0, 0.5f)]
    public float detectorPosX;

    public WorkoutParameters()
    {

    }

    public WorkoutParameters(int reps, float time, int drills)
    {
        this.reps = reps;
        this.time = time;
        this.drills = drills;
    }
    public WorkoutParameters(int reps, float time, int drills, float posX)
    {
        this.reps = reps;
        this.time = time;
        this.drills = drills;
        this.detectorPosX = posX;
    }
}
