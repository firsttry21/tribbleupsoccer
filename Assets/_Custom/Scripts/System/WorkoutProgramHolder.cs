﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class WorkoutProgramHolder : MonoBehaviour
{
    public static WorkoutProgramHolder instance;

    private int choosenWorkoutProgramId;

    private List<WorkoutDrill> allDrillsList = new List<WorkoutDrill>();
    private WorkoutProgram[] allProgramsArray;
    private DTO_WorkoutProgram[] allProgramsData;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(this.gameObject);
            return;
        }

        allProgramsArray = GetComponentsInChildren<WorkoutProgram>();
    }

    public WorkoutProgram[] getAllPrograms()
    {
        return allProgramsArray;
    }

    public DTO_WorkoutProgram[] getAllProgramsData()
    {
        allProgramsData = new DTO_WorkoutProgram[allProgramsArray.Length];
        for (int i = 0; i < allProgramsData.Length; i++)
        {
            allProgramsData[i] = (DTO_WorkoutProgram)allProgramsArray[i];
        }

        return allProgramsData;

    }

    public void setSelectedProgram(int programId)
    {
        PlayerPrefs.SetInt(GlobalValue.selectedWorkoutProgramKey, programId);
        choosenWorkoutProgramId = programId;
    }

    public DTO_WorkoutProgram getSelectedProgram(int programId)
    {
        Debug.Log("Choosen program ID: " + programId);

        if (programId >= transform.childCount)
            Debug.LogError("Make sure all workout programs are loaded properly in holder object. yaddumbbetch!");

        var program = transform.GetChild(programId).GetComponent<WorkoutProgram>();

        //load selected program's data
        DTO_WorkoutProgram selectedProgramData = PlayerDataManager.instance.getProgramDataById(programId);
        //program.daysCompleted = selectedProgramData.daysCompleted;
        //for (int i = 0; i < program.workoutDrills.Length; i++)
        //{
        //    program.workoutDrills[i].personalBest = selectedProgramData.workoutDrills[i].personalBest;
        //}

        return selectedProgramData;
    }

    public WorkoutDrill[] getAllDrills()
    {
        foreach (WorkoutProgram program in allProgramsArray)
        {
            allDrillsList.AddRange(program.workoutDrills);
        }
        return allDrillsList.ToArray();
    }

    public WorkoutDrill getSelectedDrill()
    {
        return getDrillById(PlayerPrefs.GetInt(GlobalValue.selectedPracticeDrillId, 1));
    }

    private WorkoutDrill getDrillById(int id)
    {
        if (allDrillsList != null || allDrillsList.Count != 0)
            return allDrillsList[id];

        var allWorkoutPrograms = GetComponentsInChildren<WorkoutProgram>();
        foreach (WorkoutProgram program in allWorkoutPrograms)
            allDrillsList.AddRange(program.workoutDrills);
        var allDrillsArray = allDrillsList.ToArray();
        return allDrillsArray[id];
    }

    private WorkoutDrill[] getDrillsForProgram(int id)
    {
        return allProgramsArray[id].workoutDrills;
    }
}