﻿using OpenCvSharp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class imutils
{

    public static Mat resize(Mat image, double? width = null, double? height = null, InterpolationFlags inter = InterpolationFlags.Area)
    {
        Vector2 dim = Vector2.zero;
        Size size = image.Size();

        if (width == null && height == null)
            return image;

        if (width == null)
        {
            //calculate the ratio of the height and construct the
            //dimensions
            float ratio = (float)height / size.Height;
            dim = new Vector2(size.Width * ratio, (float)height);
        }
        else
        {
            float ratio = (float)width / size.Width;
            dim = new Vector2((float)width, (float)size.Height * ratio);
        }

        Mat resized = new Mat();
        Cv2.Resize(image, resized, new Size(dim.x, dim.y));

        return resized;
    }

    public static Mat resize(Mat image, out Vector2 sizeDiff, double? width = null, double? height = null, InterpolationFlags inter = InterpolationFlags.Area)
    {
        Vector2 dim = Vector2.zero;
        Size size = image.Size();
        sizeDiff = Vector2.zero;

        if (width == null && height == null)
            return image;

        if (width == null)
        {
            //calculate the ratio of the height and construct the
            //dimensions
            float ratio = (float)height / size.Height;
            dim = new Vector2(size.Width * ratio, (float)height);
        }
        else
        {
            float ratio = (float)width / size.Width;
            dim = new Vector2((float)width, (float)size.Height * ratio);
        }

        Mat resized = new Mat();
        Cv2.Resize(image, resized, new Size(dim.x, dim.y));

        sizeDiff = new Vector2(size.Width / dim.x, size.Height / dim.y);
        return resized;
    }

}
