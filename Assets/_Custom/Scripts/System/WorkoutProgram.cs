﻿using System;
using UnityEngine;

[Serializable]
public class WorkoutProgram : MonoBehaviour
{
                      public int            programId;
                      public string         programTitle;
                      public int            totalDays;
                      public WorkoutDrill[] workoutDrills;
    [HideInInspector] public int            totalCompletedReps;
    [HideInInspector] public int            daysCompleted;
    [HideInInspector] public DateTime       dateTimeWhenCompleted;
    //[HideInInspector] public string         dateWhenCompleted;
    //[HideInInspector] public string         timeWhenCompleted;

    public static explicit operator DTO_WorkoutProgram(WorkoutProgram program) =>
        new DTO_WorkoutProgram()
        {
            programId = program.programId,
            programTitle = program.programTitle,
            totalDays = program.totalDays,
            workoutDrills = program.workoutDrills,
            totalCompletedReps = program.totalCompletedReps,
            daysCompleted = program.daysCompleted,
            dateTimeWhenCompleted = program.dateTimeWhenCompleted,
            //dateWhenCompleted = program.dateWhenCompleted,
            //timeWhenCompleted = program.timeWhenCompleted
        };
}