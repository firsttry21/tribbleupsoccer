﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
[DataContract]
public class FamilyMember
{
    [DataMember(Name = "familyId")] public int familyId;
    [DataMember(Name = "userName")] public string memberName;
}
