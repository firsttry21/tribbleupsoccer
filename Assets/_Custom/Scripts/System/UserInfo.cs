﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

[Serializable]
[DataContract]
public class UserInfo
{
    [DataMember(Name = "token")]         public string tokenId { get; set; }
    [DataMember(Name = "userId")]        public int userId { get; set; }
    [DataMember(Name = "emailAddress")]  public string emailAddress { get; set; }
    [DataMember(Name = "userName")]      public string username { get; set; }
    [DataMember(Name = "familyMembers")] public List<FamilyMember> membersList = new List<FamilyMember>();

                                         public string firstName { get; set; }
                                         public string lastName { get; set; }
                                         public DateTime? dob { get; set; }
                                         public string password { get; set; }
}
