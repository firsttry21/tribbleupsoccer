﻿using System.Collections;
using UnityEngine;

public class AutoHideScript : MonoBehaviour
{
    public float disableAfter = 1f;

    private void OnEnable()
    {
        StartCoroutine(disableGameObjectAfterCo(disableAfter));
    }

    IEnumerator disableGameObjectAfterCo(float duration)
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive(false);
    }
}