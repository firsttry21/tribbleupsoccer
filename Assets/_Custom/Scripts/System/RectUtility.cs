﻿using UnityEngine;

namespace RectExtension
{
    public static class RectUtility
    {
        public static bool isRectOverlappingCheckByDistance(this RectTransform rectTransform1, RectTransform rectTransform2)
        {
            return Vector2.Distance(rectTransform1.localPosition, rectTransform2.localPosition) <
                (rectTransform1.sizeDelta.x / 2) + ((rectTransform2.sizeDelta.x / 2) / 4);
        }

        public static bool isRectOverlappingCheckByBounds(this RectTransform rectTransform1, RectTransform rectTransform2)
        {
            Rect rect1 = new Rect(rectTransform1.localPosition.x, rectTransform1.localPosition.y, rectTransform1.rect.width, rectTransform1.rect.height);
            Rect rect2 = new Rect(rectTransform2.localPosition.x, rectTransform2.localPosition.y, rectTransform2.rect.width, rectTransform2.rect.height);

            return rect1.Overlaps(rect2, true);
        }
    }
}