﻿
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;

public class SoccerWebApi : MonoBehaviour
{
    public static SoccerWebApi instance;

    public string noInternetErrorMessage = "Please connect to the internet and try again.";
    public string notFoundErrorMessage = "Error: 404 Not Found. It's the dev's fault. Those idiots lol";
    public string emailTakenErrorMessage = "This email address is already taken. Are you trying to log in?";
    public string usernameTakenErrorMessage = "Username already taken. Please choose another one.";

    readonly string soccerApiAccount = "http://soccerapi.imedhealth.us/Account/";
    readonly string soccerApiPlayer = "http://soccerapi.imedhealth.us/Player/";

    readonly string loginOperationString = "loginUser";
    readonly string sendResetEmailOperationString = "forgotPassword?email=";
    readonly string confirmResetPasswordCodeOperationString = "confirmForgotPasswordCode";
    readonly string resetPasswordOperationString = "resetForgottenPassword";

    readonly string signupOperationString = "registerUser";
    readonly string registerFamilyMemberOperationString = "registerFamilyMember?UserName=";
    readonly string checkAvailableEmailOrUsernameString = "IsEmailOrUserNameExist?userInput=";
    readonly string verifyInvitationCodeOperationString = "VerifyInvitationCode";

    readonly string getPlayerDataOperationString = "getPlayerData?playerId=";
    readonly string uploadPlayerDataOperationString = "postPlayerData";

    private string userAccessToken = "";

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    #region operations methods

    public void postLoginRequest(string username, string password, Action<UserInfo> onLoginSuccess, Action<string> onLoginFail)
    {
        StartCoroutine(postUserLoginCo(username, password, onLoginSuccess, onLoginFail));
    }

    public void postRetrieveDataRequest(int memberId)
    {
        StartCoroutine(requestMemberDataCo(memberId));
    }

    public void isEmailOrUsernameAvailable(SignupStep currentStep, string signupInput, Action<SignupStep> onSignupStepSuccess, Action<SignupStep, string> onSignupStepFail)
    {
        StartCoroutine(isEmailOrUsernameAvailableCo(currentStep, signupInput, onSignupStepSuccess, onSignupStepFail));
    }

    public void checkForOtpMatch(SignupStep currentStep, string email, string otp, Action<SignupStep> onSignupStepSuccess, Action<SignupStep, string> onSignupStepFail)
    {
        StartCoroutine(postOtpMatchRequestCo(currentStep, email, otp, onSignupStepSuccess, onSignupStepFail));
    }

    public void postSignupRequest(UserInfo signupInfo, Action<UserInfo> onSignupSuccess, Action<string> onSignupFail)
    {
        StartCoroutine(postSignupRequestCo(signupInfo, onSignupSuccess, onSignupFail));
    }

    public void postCreateNewFamilyMemberRequest(string newUsername, Action onCreateNewFamilyMemberSuccess, Action<string> onCreateNewFamilyMemberFail)
    {
        StartCoroutine(createNewFamilyMemberCo(newUsername, onCreateNewFamilyMemberSuccess, onCreateNewFamilyMemberFail));
    }

    public void postProgramDataUploadRequest(ProgramServerCache data)
    {
        StartCoroutine(uploadPlayerDataCo(data));
    }

    public void sendResetPasswordEmail(string email, Action onSendPasswordResetEmailSuccess, Action<string> onSendPasswordResetEmailFail)
    {
        StartCoroutine(sendResetPasswordEmailCo(email, onSendPasswordResetEmailSuccess, onSendPasswordResetEmailFail));
    }

    public void matchResetPasswordCode(string resetRequestEmail, string code, Action onCodeMatchSuccess, Action<string> onCodeMatchFail)
    {
        StartCoroutine(matchResetPasswordCodeCo(resetRequestEmail, code, onCodeMatchSuccess, onCodeMatchFail));
    }

    public void postResetPasswordRequest(string email, string newPassword, Action onResetPasswordSuccess, Action<string> onResetPasswordFail)
    {
        StartCoroutine(setNewPasswordCo(email, newPassword, onResetPasswordSuccess, onResetPasswordFail));
    }

    #endregion

    IEnumerator postUserLoginCo(string usrname, string passwd, Action<UserInfo> onLoginSuccess, Action<string> onLoginFail)
    {
        Dictionary<string, string> loginData = new Dictionary<string, string>();
        loginData.Add("userInput", usrname);
        loginData.Add("password", passwd);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + loginOperationString, loginData))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                //TODO remove debugs and clean up
                Debug.Log("Log in Error: " + webRequest.responseCode + " " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onLoginFail(noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onLoginFail(notFoundErrorMessage);
                }

                yield break;
            }
            else
            {
                string loginResponseJson = webRequest.downloadHandler.text;
                //TODO (THIS) what if its null??????
                BasicOperationData<UserInfo> loginResponseData = JsonConvert.DeserializeObject<BasicOperationData<UserInfo>>(loginResponseJson);

                if (loginResponseData.status == 1)
                {
                    //TODO remove debugs
                    userAccessToken = loginResponseData.data.tokenId;
                    onLoginSuccess(loginResponseData.data);
                }
                else
                {
                    onLoginFail(loginResponseData.message);
                }
            }
        }
    }

    IEnumerator requestMemberDataCo(int memberId)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiPlayer + getPlayerDataOperationString + memberId.ToString(), memberId.ToString()))
        {
            // Request and wait for the desired page.
            webRequest.SetRequestHeader("Authorization", "Bearer " + userAccessToken);
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                //TODO remove debugs and clean up
                Debug.Log("Data Retrieve Error: " + webRequest.responseCode + " " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    MainMenuManager.instance.onRetrieveDataFail(noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    MainMenuManager.instance.onRetrieveDataFail(notFoundErrorMessage);
                }

                yield break;
            }
            else
            {
                string retrieveDataResponseJson = webRequest.downloadHandler.text;
                BasicOperationData<DTO_PlayerData> retrieveDataResponseObject =
                    JsonConvert.DeserializeObject<BasicOperationData<DTO_PlayerData>>(retrieveDataResponseJson);
                //TODO (THIS) what if its null??????

                Debug.Log("status=" + retrieveDataResponseObject.status);
                if (retrieveDataResponseObject.status == 1) //login was successful
                {
                    Debug.Log("memberData=" + retrieveDataResponseObject.data);
                    MainMenuManager.instance.onRetrieveDataSucceed(retrieveDataResponseObject.data);
                }
                else
                {
                    MainMenuManager.instance.onRetrieveDataFail(retrieveDataResponseObject.message);
                }
            }
        }
    }

    IEnumerator isEmailOrUsernameAvailableCo(SignupStep currentStep, string input, Action<SignupStep> onSignupStepSuccess, Action<SignupStep, string> onSignupStepFail)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + checkAvailableEmailOrUsernameString + input, input))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log("Unable to check: " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onSignupStepFail(currentStep, noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onSignupStepFail(currentStep, notFoundErrorMessage);
                }
                yield break;
            }
            else
            {
                string isEmailAvailableResponse = webRequest.downloadHandler.text;
                Debug.Log("isEmailAvailableResponse:\n" + webRequest.downloadHandler.text);
                BasicOperationData<bool> isEmailAvailableResponseData = JsonConvert.DeserializeObject<BasicOperationData<bool>>(isEmailAvailableResponse);

                if (isEmailAvailableResponseData.status == 1)
                {
                    onSignupStepSuccess(currentStep);
                }
                else
                {
                    onSignupStepFail(currentStep, currentStep == SignupStep.Email ? emailTakenErrorMessage : usernameTakenErrorMessage);
                }
            }
        }
    }

    IEnumerator postOtpMatchRequestCo(SignupStep currentStep, string email, string otp, Action<SignupStep> onSignupStepSuccess, Action<SignupStep, string> onSignupStepFail)
    {
        Dictionary<string, string> otpRequestData = new Dictionary<string, string>();
        otpRequestData.Add("email", email);
        otpRequestData.Add("invitationCode", otp);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + verifyInvitationCodeOperationString, otpRequestData))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log("Unable to check: " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onSignupStepFail(currentStep, noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onSignupStepFail(currentStep, notFoundErrorMessage);
                }
                yield break;
            }
            else
            {
                string otpMatchResponseJson = webRequest.downloadHandler.text;
                BasicOperationData<bool> otpMatchResponseObject = JsonConvert.DeserializeObject<BasicOperationData<bool>>(otpMatchResponseJson);

                if (otpMatchResponseObject.status == 1)
                {
                    onSignupStepSuccess(currentStep);
                }
                else
                {
                   onSignupStepFail(currentStep, otpMatchResponseObject.message);
                }
            }
        }
    }

    IEnumerator postSignupRequestCo(UserInfo signupInfo, Action<UserInfo> onSignupSuccess, Action<string> onSignupFail)
    {
        Dictionary<string, string> signupData = new Dictionary<string, string>();
        signupData.Add("firtName", signupInfo.firstName);
        signupData.Add("lastName", signupInfo.lastName);
        signupData.Add("dateOfBirth", signupInfo.dob.ToString());
        signupData.Add("userName", signupInfo.username);
        signupData.Add("emailAddress", signupInfo.emailAddress);
        signupData.Add("password", signupInfo.password);
        signupData.Add("profileURL", string.Empty);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + signupOperationString, signupData))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log("Signup failed: " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onSignupFail(noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onSignupFail(notFoundErrorMessage);
                }

                yield break;
            }
            else
            {
                string signupResponseJson = webRequest.downloadHandler.text;
                BasicOperationData<UserInfo> signupResponseData = JsonConvert.DeserializeObject<BasicOperationData<UserInfo>>(signupResponseJson);

                if (signupResponseData.status == 1)
                {
                    Debug.Log("signup success");
                    userAccessToken = signupResponseData.data.tokenId;
                    onSignupSuccess(signupResponseData.data);
                }
                else
                {
                    onSignupFail(signupResponseData.message);
                    yield break;
                }
            }
        }
    }

    IEnumerator createNewFamilyMemberCo(string newUsername, Action onCreateNewFamilyMemberSuccess, Action<string> onCreateNewFamilyMemberFail)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + registerFamilyMemberOperationString + newUsername, newUsername))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + userAccessToken);
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log("Failed: " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onCreateNewFamilyMemberFail(noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onCreateNewFamilyMemberFail(notFoundErrorMessage);
                }
                yield break;
            }
            else
            {
                string createMemberResponseJson = webRequest.downloadHandler.text;
                BasicOperationData<bool> createMemberResponse = JsonConvert.DeserializeObject<BasicOperationData<bool>>(createMemberResponseJson);

                if (createMemberResponse.status == 1)
                {
                    Debug.Log("Created: " + newUsername);
                    onCreateNewFamilyMemberSuccess();
                }
                else
                {
                    Debug.Log("Failed: " + createMemberResponse.message);
                    onCreateNewFamilyMemberFail(createMemberResponse.message);
                }
            }
        }
    }

    IEnumerator uploadPlayerDataCo(ProgramServerCache data)
    {
        Dictionary<string, string> dataToUpload = new Dictionary<string, string>();
        dataToUpload.Add("playerId", data.familyId.ToString());
        dataToUpload.Add("selectProgramId", data.selectedProgramId.ToString());
        dataToUpload.Add("maxJuggles", data.juggleHighScore.ToString());
        dataToUpload.Add("unsavedPrograms", JsonConvert.SerializeObject(data.unsavedPrograms));

        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiPlayer + uploadPlayerDataOperationString, dataToUpload))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + userAccessToken);
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                //TODO remove debugs and clean up
                Debug.Log("UPLOAD ERROR: " + webRequest.responseCode + " " + webRequest.error);
                yield break;
            }
            //else
            //{
            //    string dataUploadResponseJson = webRequest.downloadHandler.text;
            //    //TODO (THIS) what if its null??????
            //    Debug.Log("dataUploadResponse:\n" + webRequest.downloadHandler.text);
            //    BasicOperationData<bool> dataUploadResponse = JsonConvert.DeserializeObject<BasicOperationData<bool>>(dataUploadResponseJson);

            //    if (dataUploadResponse.status == 1)
            //    {
            //        Debug.Log("UPLOAD DONE");
            //    }
            //    else
            //    {
            //        Debug.Log("UPLOAD FAIL" + dataUploadResponse.message);
            //    }
            //}
        }
    }

    IEnumerator sendResetPasswordEmailCo(string email, Action onSendEmailSuccess, Action<string> onSendEmailFail)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + sendResetEmailOperationString + email, email))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log("Failed: " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onSendEmailFail(noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onSendEmailFail(notFoundErrorMessage);
                }
                yield break;
            }
            else
            {
                string sendEmailResponseJson = webRequest.downloadHandler.text;
                BasicOperationData<bool> sendEmailResponse = JsonConvert.DeserializeObject<BasicOperationData<bool>>(sendEmailResponseJson);

                if (sendEmailResponse.status == 1)
                {
                    Debug.Log("Sent: " + email);
                    onSendEmailSuccess();
                }
                else
                {
                    Debug.Log("Failed: " + sendEmailResponse.message);
                    onSendEmailFail(sendEmailResponse.message);
                }
            }
        }
    }

    IEnumerator matchResetPasswordCodeCo(string userEmail, string enteredCode, Action onCodeMatchSuccess, Action<string> onCodeMatchFail)
    {
        Dictionary<string, string> requestData = new Dictionary<string, string>();
        requestData.Add("email", userEmail);
        requestData.Add("invitationCode", enteredCode);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + confirmResetPasswordCodeOperationString, requestData))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log("Failed: " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onCodeMatchFail(noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onCodeMatchFail(notFoundErrorMessage);
                }
                yield break;
            }
            else
            {
                string codeMatchResponseJson = webRequest.downloadHandler.text;
                BasicOperationData<bool> codeMatchResponse = JsonConvert.DeserializeObject<BasicOperationData<bool>>(codeMatchResponseJson);

                if (codeMatchResponse.status == 1)
                {
                    Debug.Log("MATCH!! ");
                    onCodeMatchSuccess();
                }
                else
                {
                    Debug.Log("Failed: " + codeMatchResponse.message);
                    onCodeMatchFail(codeMatchResponse.message);
                }
            }
        }
    }
 
    IEnumerator setNewPasswordCo(string userEmail, string newPassword, Action onChangePasswordSuccess, Action<string> onChangePasswordFail)
    {
        Dictionary<string, string> requestData = new Dictionary<string, string>();
        requestData.Add("email", userEmail);
        requestData.Add("newPassword", newPassword);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(soccerApiAccount + resetPasswordOperationString, requestData))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + userAccessToken);
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.Log("Failed: " + webRequest.error);
                if (webRequest.responseCode == 0)
                {
                    onChangePasswordFail(noInternetErrorMessage);
                }
                if (webRequest.responseCode == 404)
                {
                    onChangePasswordFail(notFoundErrorMessage);
                }
                yield break;
            }
            else
            {
                string codeMatchResponseJson = webRequest.downloadHandler.text;
                BasicOperationData<bool> codeMatchResponse = JsonConvert.DeserializeObject<BasicOperationData<bool>>(codeMatchResponseJson);

                if (codeMatchResponse.status == 1)
                {
                    Debug.Log("MATCH!! ");
                    onChangePasswordSuccess();
                }
                else
                {
                    Debug.Log("Failed: " + codeMatchResponse.message);
                    onChangePasswordFail(codeMatchResponse.message);
                }
            }
        }
    }
}
