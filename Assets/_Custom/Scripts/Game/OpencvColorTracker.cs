﻿using OpenCvSharp;
using OpenCvSharp.Demo;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class OpencvColorTracker : WebCamera
{
    #region Declarations

    [Range(1, 15)]
    public int blurSize = 11;

    public CameraSelection camType = CameraSelection.BACK;

    [Space]
    public Text deviceNameText;

    [Range(1, 5)]
    public int dileroIterations = 2;

    // downscaling const
    [Range(0.33f, 1.2f)]
    public float downScale = 1f;

    [Header("Color Lower Threshold")]
    [Range(0f, 255f)]
    public float lh = 21f;
    [Range(0f, 255f)]
    public float ls = 144f;
    [Range(0f, 255f)]
    public float lv = 0f;
    [Header("Color Upper Threshold")]
    [Range(0f, 255f)]
    public float uh = 32f;
    [Range(0f, 255f)]
    public float us = 255f;
    [Range(0f, 255f)]
    public float uv = 255f;

    [Space]
    public float minDetectionRadius = 36;
    public OutputImage outputImage = OutputImage.IMAGE;

    private Point circleCenter = new Point(0.0, 0.0);
    private float detectionCircleRadius = 0;
    private Mat image;

    //color bounds in hsv
    private Scalar lowerYellow;
    private Scalar upperYellow;

    #endregion Declarations

    protected override void Awake()
    {
        base.Awake();

        forceFrontalCamera = false;
    }

    private void Start()
    {
        lowerYellow = new Scalar(21, 144, 0);
        upperYellow = new Scalar(32, 255, 255);

        setCamera(camType);
    }

    public void setCamera(CameraSelection camType)
    {
        if (camType == CameraSelection.FRONT)
        {
            foreach (var device in WebCamTexture.devices)
            {
                if (device.isFrontFacing)
                {
                    DeviceName = device.name;
                }
            }
        }
        else
        {
            DeviceName = WebCamTexture.devices[0].name;
        }
    }

    /// </summary>
    /// <param name="input"></param>
    /// <param name="output"></param>
    /// <returns></returns>
    protected override bool ProcessTexture(WebCamTexture input, ref Texture2D output)
    {
        output = findCircleWithFindContours(input, output);

        return true;
    }

    private Texture2D findCircleWithFindContours(WebCamTexture input, Texture2D output)
    {
        //lowerYellow = new Scalar(lh, ls, lv);
        //upperYellow = new Scalar(uh, us, uv);

        image = OpenCvSharp.Unity.TextureToMat(input, TextureParameters);
        Size originalSize = image.Size();

        Vector2 sizeDifference = Vector2.zero;
        Mat downscaled = imutils.resize(image, out sizeDifference, 600);

        // Clean up image using Gaussian Blur
        Mat blurred = new Mat();
        Cv2.GaussianBlur(downscaled, blurred, new Size(blurSize, blurSize), 0);

        //Convert image to hsv
        Mat hsv = new Mat();
        Cv2.CvtColor(downscaled, hsv, ColorConversionCodes.BGR2HSV);

        //construct a mask for the color "yellow", then perform
        //a series of dilations and erosions to remove any small
        //blobs left in the mask
        Mat mask = new Mat();
        Cv2.InRange(hsv, lowerYellow, upperYellow, mask);
        Cv2.Erode(mask, mask, new Mat(), null, 2);
        Cv2.Dilate(mask, mask, new Mat(), null, 2);

        Mat[] contours;
        Mat hierarchy = new Mat();
        Cv2.FindContours(mask, out contours, hierarchy, RetrievalModes.External, ContourApproximationModes.ApproxSimple, null);

        if (contours.Length > 0)
        {
            double max = 0;
            Mat biggestCont = new Mat();
            foreach (Mat contour in contours)
            {
                if (Cv2.ContourArea(contour) >= max)
                {
                    max = Cv2.ContourArea(contour);
                    biggestCont = contour;
                }
            }
            Point2f center = new Point2f(0, 0);
            Cv2.MinEnclosingCircle(biggestCont, out center, out detectionCircleRadius);
            detectionCircleRadius = detectionCircleRadius * sizeDifference.x;
            if (detectionCircleRadius > minDetectionRadius)
            {
                Moments m = Cv2.Moments(biggestCont);
                int cx = (int)(m.M10 / m.M00);
                int cy = (int)(m.M01 / m.M00);

                center = new Point2f(cx * sizeDifference.x, cy * sizeDifference.y);

                circleCenter = new Point(center.X, center.Y);

                if (GameManager.instance.state == GameManager.GameState.Starting)
                {
                    Cv2.Circle(image, circleCenter, (int)detectionCircleRadius, new Scalar(0, 255, 255), 5);
                    DetectionManager.instance.drawDetectionCircle(image.Size(), new Vector2(circleCenter.X, circleCenter.Y), detectionCircleRadius);
                    return OpenCvSharp.Unity.MatToTexture(image, output);
                }

                DetectionManager.instance.drawDetectionCircle(image.Size(), new Vector2(circleCenter.X, circleCenter.Y), detectionCircleRadius);
            }
            else
            {
                DetectionManager.instance.showHideDetectionCircle(false);
            }
        }

        //opencvUi(circleCenter);

        return OpenCvSharp.Unity.MatToTexture(image, output);
    }

    private double getDistanceBetweenPoints(Point p1, Point p2)
    {
        return Vector2.Distance(new Vector2(p1.X, p1.Y), new Vector2(p2.X, p2.Y));
    }

    private void opencvUi(Point circle)
    {
        Point p1;
        Point p2;
        int uiCircleRadius = 60;
        Scalar defaultColorValue = new Scalar(255, 10, 10);
        Scalar detectionColorValue = new Scalar(10, 255, 10);

        p1 = new Point(image.Size().Width * 0.20, image.Size().Height * 0.5);
        p2 = new Point(image.Size().Width * 0.80, image.Size().Height * 0.5);
        double distanceFrom_p1 = getDistanceBetweenPoints(p1, circleCenter);
        double distanceFrom_p2 = getDistanceBetweenPoints(p2, circleCenter);
        if (distanceFrom_p1 < (uiCircleRadius + detectionCircleRadius) - 5)
        {
            Cv2.Circle(image, p1, uiCircleRadius, detectionColorValue, 15);
            Cv2.Circle(image, p2, uiCircleRadius, defaultColorValue, 15);
        }
        else if (distanceFrom_p2 < (uiCircleRadius + detectionCircleRadius) - 5)
        {
            Cv2.Circle(image, p1, uiCircleRadius, defaultColorValue, 15);
            Cv2.Circle(image, p2, uiCircleRadius, detectionColorValue, 15);
        }
        else
        {
            Cv2.Circle(image, p1, uiCircleRadius, defaultColorValue, 15);
            Cv2.Circle(image, p2, uiCircleRadius, defaultColorValue, 15);
        }
    }
}

public enum CameraSelection
{
    FRONT = 0,
    BACK
}

public enum OutputImage
{
    DOWNSCALED,
    BLURRED,
    HSV,
    MASK,
    IMAGE
}