﻿using RectExtension;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System;

public class JuggleModeDetectionCircle : MonoBehaviour
{
    public float travelDistanceBeforeAllowScore = 3f;
    public float idleScoreResetTimeout = 2f;
    public float killZoneScoreResetTimeout = .5f;

    private enum MoveState { Ascending, Descending }
    private MoveState moveState;

    private RectTransform juggleDetectionCircle;
    private RectTransform jugglesKillZone;

    private Vector2 previousPosition;
    private MoveState previousMoveState;
    private Vector2 lastStartingPosition;
    private bool canGiveScore;
    private int tempScore = 0;
    private float idleTimer;
    private float killZonetimer;

    private void Start()
    {
        juggleDetectionCircle = GetComponent<RectTransform>();
        jugglesKillZone = DetectionManager.instance.jugglesKillZone.GetComponent<RectTransform>();

        canGiveScore = false;
        lastStartingPosition = previousPosition = transform.position;
        moveState = MoveState.Ascending;
        previousMoveState = MoveState.Descending;
        reset();
    }

    private void reset()
    {
        tempScore = 0;
        GameManager.instance.gainedJuggleScore = 0;
        canGiveScore = false;
        idleTimer = 0;
        killZonetimer = 0;
    }

    private void OnDisable()
    {
        canGiveScore = false;
    }

    private void Update()
    {
        idleTimer += Time.deltaTime;

        if (juggleDetectionCircle.isRectOverlappingCheckByBounds(jugglesKillZone))
        {
            killZonetimer += Time.deltaTime;
            if (killZonetimer > killZoneScoreResetTimeout)
            {
                GameManager.instance.gainedJuggleScore = 0;
                tempScore = 0;
                killZonetimer = 0;
            }
        }

        if (idleTimer > idleScoreResetTimeout)
        {
            GameManager.instance.gainedJuggleScore = 0;
            tempScore = 0;
        }

        if (moveState == MoveState.Ascending)
        {
            Debug.Log("A S C E N D I N G");
            if (Mathf.Abs(((Vector2)transform.position - lastStartingPosition).y) > travelDistanceBeforeAllowScore &&
                                                                        previousMoveState == MoveState.Descending)
                lastStartingPosition = transform.position;
            if (Mathf.Abs(((Vector2)transform.position - lastStartingPosition).y) > travelDistanceBeforeAllowScore &&
                                                       ((Vector2)transform.position - lastStartingPosition).y > 0)
                canGiveScore = true;
            if (canGiveScore)
            {
                if (Mathf.Abs(((Vector2)transform.position - lastStartingPosition).y) < travelDistanceBeforeAllowScore)
                {
                    tempScore++;
                    if ((tempScore % 2) == 0)
                        GameManager.instance.addJuggleScore(1);
                    idleTimer = 0;
                    lastStartingPosition = transform.position;
                    canGiveScore = false;
                }
            }
            previousMoveState = MoveState.Ascending;
        }

        if (moveState == MoveState.Descending)
        {
            Debug.Log("D E S C E N D I N G");
            previousMoveState = MoveState.Descending;
        }

        if (transform.position.y > previousPosition.y)
            moveState = MoveState.Ascending;
        else if (transform.position.y < previousPosition.y)
            moveState = MoveState.Descending;
        previousPosition = transform.position;



        #region juggle pseudo code
        //if (isTimerDone() && GameManager.instance.gainedJuggleScore == lastJuggleScore)
        //    GameManager.instance.gainedJuggleScore = 0;

        //if (isBallTouchingKillZone)
        //    GameManager.instance.gainedJuggleScore = 0;

        //if (isBallOutOfKillZone)
        //{
        //    startingPosition = ballPosition;
        //    noteVerticalDistance;
        //    if (ballVerticalDirectionInverted)
        //    {
        //        if (Vector2.Distance(lastStartingPoint, ballPosition) < giveJuggleScoreMargin)
        //        {
        //            GameManager.instance.addJuggleScore(1);
        //        }
        //    }
        //}
        #endregion
    }
}