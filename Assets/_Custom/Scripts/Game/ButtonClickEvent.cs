﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonClickEvent : MonoBehaviour
{
    #region Public Fields
    public string eventName;
    #endregion

    #region Unity Methods
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(() =>
        {
            EventManager.TriggerEvent(eventName);
        });
    }

    void Update()
    {

    }
    #endregion

    #region Private Methods

    #endregion
}