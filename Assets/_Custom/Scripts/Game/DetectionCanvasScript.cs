﻿using OpenCvSharp;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(DetectionManager))]
public class DetectionCanvasScript : MonoBehaviour
{
    [HideInInspector] public GameObject detectorWhereBallWasDetectedLastTime;
    public RectTransform imageRect;
    public RectTransform detectorRect;
    public float minTravelDistance = 5f;
    public float moveSmoothing = 9f;
    public float radiusSmoothing = 10f;
    public float detectionLossCooldown = 3;
    [Space]
    public float allowedDetectionRadiusMin = 40f;
    public float allowedDetectionRadiusMax = 400f;
    public Color detectionPausedColor = Color.gray;

    [Space]
    [Range(0.0f, 0.5f)]
    public float detectionPos_x;

    [Range(-0.5f, 0.5f)]
    public float detectionPos_y;

    [Range(0.0f, 1.0f)]
    public float detectionCircleSize;

    private bool hasDetected = false;
    private bool isCooling = false;
    private CanvasScaler canvasScaler;
    private Image detectorRectImage;
    private Color defaultColor;
    private float lastTime = -1;
    private bool canChangeRadius = true;

    private void Start()
    {
        detectorRectImage = detectorRect.GetComponent<Image>();
        defaultColor = detectorRectImage.color;

        hasDetected = false;
        isCooling = false;
    }

    private void OnEnable()
    {
        //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//H A C K S\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
        setDetectionUi();
        //\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//H A C K S\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
    }

    private void OnDisable()
    {
        canChangeRadius = true;
        isCooling = false;
        hasDetected = false;

        StopAllCoroutines();
    }

    private void setDetectionUi()
    {
        canvasScaler = this.GetComponentInParent<CanvasScaler>();

        if (canvasScaler != null)
            canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

        var childDetectors = GetComponentsInChildren<UiDetector>();
        int posMult = -1;
        foreach (UiDetector childDetector in childDetectors)
        {
            var currentDetector = childDetector.GetComponent<RectTransform>();
            currentDetector.anchoredPosition = new Vector2(posMult * (detectionPos_x * Screen.width), detectionPos_y * Screen.height);
            currentDetector.sizeDelta = new Vector2(detectionCircleSize * Screen.height, detectionCircleSize * Screen.height);
            posMult *= -1;
        }
    }

    public void setDetectionUi(float posX)
    {
        canvasScaler = this.GetComponentInParent<CanvasScaler>();

        if (canvasScaler != null)
            canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

        var childDetectors = GetComponentsInChildren<UiDetector>();
        int posMult = -1;
        foreach (UiDetector childDetector in childDetectors)
        {
            var currentDetector = childDetector.GetComponent<RectTransform>();
            currentDetector.anchoredPosition = new Vector2(posMult * (posX * Screen.width), detectionPos_y * Screen.height);
            currentDetector.sizeDelta = new Vector2(detectionCircleSize * Screen.height, detectionCircleSize * Screen.height);
            posMult *= -1;
        }
    }

    public void drawUiCircle(Size imageSize, Vector2 center, float radius)
    {
        float delta = getFunctionDeltaTime();

        var previousPosition = detectorRect.anchoredPosition;
        var prevRadius = detectorRect.sizeDelta.x / 2;

        //if (!detectorRect.gameObject.activeSelf) detectorRect.gameObject.SetActive(true);

        #region calculations summary
        //canvas to imageRect adjsutment
        //imageRect to image adsjustment
        //apply adjsutment to detectorRect for posittioning it
        // (applied below)
        #endregion calculations summary

        Vector2 calculatedPosition = getCircleCenterBasedOnCircleSize(imageSize, center, radius);

        ///// ANTI-FLICKER ALGORITHMIC SEQUENCE ////
        var distanceFromPreviousPosition = Vector2.Distance(previousPosition, calculatedPosition);
        //OpencvColorTracker.instance.deviceNameText.text = String.Format(OpencvColorTracker.instance.deviceNameText.text + " • Distance: " + distanceFromPreviousPosition.ToString("00.00"));
        if (distanceFromPreviousPosition <= minTravelDistance && hasDetected)
        {
            setDetectorRectActive(true);
            detectorRect.anchoredPosition = Vector2.Lerp(detectorRect.anchoredPosition, calculatedPosition, moveSmoothing * delta);
        }
        else if (distanceFromPreviousPosition > minTravelDistance)
        {
            setDetectorRectActive(false);
            if (!isCooling)
            {
                isCooling = true;
                StartCoroutine(startDetectionCooldownCo(detectionLossCooldown + UnityEngine.Random.Range(.5f, 1.5f)));
            }
        }
        if (!hasDetected)
        {
            hasDetected = true;
            setDetectorRectActive(true);
            detectorRect.anchoredPosition = calculatedPosition;
        }

        //detectorRectRadiusCheck(radius);
    }

    //// show move farther/closer message
    private void detectorRectRadiusCheck(float radius)
    {
        if (radius <= (allowedDetectionRadiusMax) && radius >= (allowedDetectionRadiusMin))
        {
            GameManager.instance.state = GameManager.GameState.Playing;
            MenuManager.instance.showHideDetectionWarning(false);
            detectorRectImage.color = defaultColor;
        }
        else
        {
            GameManager.instance.state = GameManager.GameState.DetectionPaused;
            MenuManager.instance.showHideDetectionWarning(true, (radius < allowedDetectionRadiusMin) ? true : false);
            detectorRectImage.color = detectionPausedColor;
        }
    } //// end 

    private Vector2 getCircleCenterBasedOnCircleSize(Size imageSize, Vector2 center, float radius)
    {
        var horizontalAdjustment = this.GetComponent<RectTransform>().sizeDelta.x / imageSize.Width;
        var verticalAdjustment = this.GetComponent<RectTransform>().sizeDelta.y / imageSize.Height;

        //set size conditionally
        if (canChangeRadius)
            setCircleSize(2 * ((radius + 4) * horizontalAdjustment));

        Vector2 circleCenterInScreenSpcae = getCircleCenterInScreenSpace(center);

        float calculated_x = circleCenterInScreenSpcae.x * horizontalAdjustment;
        float calculated_y = circleCenterInScreenSpcae.y * verticalAdjustment;
        Vector2 calculatedPosition = new Vector2(calculated_x, calculated_y);
        return calculatedPosition;
    }

    private float getFunctionDeltaTime()
    {
        float delta = 0;
        if (lastTime > 0)
        {
            delta = Time.time - lastTime;
            //Debug.Log("Delta between last call = " + delta);
        }
        lastTime = Time.time;
        return delta;
    }


    private void setCircleSize(float width)
    {
        if (gameObject.activeInHierarchy)
        {
            this.StartCoroutine(radiusSmoothingCo(detectorRect.sizeDelta, new Vector2(width, width), radiusSmoothing));
            this.StartCoroutine(radiusWaitTimerCo(1f));
        }
    }

    private Vector2 getCircleCenterInScreenSpace(Vector2 center)
    {
        Vector3 redCircleCenterInWorldPoint = Vector3.zero;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(imageRect, center, null, out redCircleCenterInWorldPoint);
        Vector2 redCircleCenter = RectTransformUtility.WorldToScreenPoint(null, redCircleCenterInWorldPoint);
        Vector2 circlePosition = new Vector2(redCircleCenter.x, -redCircleCenter.y);
        return circlePosition;
    }

    private IEnumerator radiusWaitTimerCo(float duration)
    {
        canChangeRadius = false;
        yield return new WaitForSecondsRealtime(duration + UnityEngine.Random.Range(-1f, 1f));
        canChangeRadius = true;
    }

    private IEnumerator radiusSmoothingCo(Vector2 from, Vector2 to, float dur)
    {
        float elapsedTime = 0;
        while (elapsedTime < dur)
        {
            detectorRect.sizeDelta = Vector2.Lerp(detectorRect.sizeDelta, to, elapsedTime / dur);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        //Debug.Log("radius smoothed......");
    }

    private IEnumerator startDetectionCooldownCo(float dur)
    {
        yield return new WaitForSecondsRealtime(dur);
        hasDetected = false;
        isCooling = false;
    }


    internal void setDetectorRectActive(bool status)
    {
        detectorRect.gameObject.SetActive(status);
    }

    private void endGame()
    {
        setDetectorRectActive(false);
    }
}