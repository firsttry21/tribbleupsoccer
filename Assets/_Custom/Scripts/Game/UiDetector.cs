﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using RectExtension;

public class UiDetector : MonoBehaviour
{
    public RectTransform trackerRect;
    public Color colorOnDetection;
    public Color colorWhenEnabled;
    public Color colorWhenDisabled;
    private RectTransform detectorRect;
    public GameObject animationElement;

    private DetectionCanvasScript parent;
    private bool hasExited;

    private void Awake()
    {
        parent = GetComponentInParent<DetectionCanvasScript>();
        if (!parent) Debug.Log("UiDetection must be child of DetectionCanvasScript instance");
    }

    private void OnEnable()
    {
        detectorRect = this.GetComponent<RectTransform>();
        resetAnimationElement();
    }

    private void Update()
    {
        if (GameManager.instance.state != GameManager.GameState.Playing) return;

        if (detectorRect.isRectOverlappingCheckByDistance(trackerRect) && trackerRect.gameObject.activeSelf && hasExited)
        {
            if (!parent.detectorWhereBallWasDetectedLastTime.Equals(this.gameObject))
            {
                hasExited = false;
                detectorRect.GetComponent<Image>().color = colorOnDetection;
                resetAnimationElement();
                animationElement.SetActive(true);

                parent.detectorWhereBallWasDetectedLastTime = this.gameObject;
                GameManager.instance.addWorkoutScore(1);
            }
        }
        else if (!detectorRect.isRectOverlappingCheckByDistance(trackerRect) || !trackerRect.gameObject.activeSelf)
        {
            if (parent.detectorWhereBallWasDetectedLastTime.Equals(this.gameObject))
            {
                detectorRect.GetComponent<Image>().color = colorWhenDisabled;
            }
            else
            {
                detectorRect.GetComponent<Image>().color = colorWhenEnabled;
            }
            hasExited = true;
        }
    }

    private void resetAnimationElement()
    {
        var animationRect = animationElement.GetComponent<RectTransform>();
        animationRect.anchorMax = detectorRect.anchorMax;
        animationRect.anchorMin = detectorRect.anchorMin;
        animationRect.pivot = detectorRect.pivot;
        animationRect.anchoredPosition = detectorRect.anchoredPosition;
        animationRect.sizeDelta = detectorRect.sizeDelta;
    }
}