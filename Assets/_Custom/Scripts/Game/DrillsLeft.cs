﻿using UnityEngine;
using UnityEngine.UI;

public class DrillsLeft : MonoBehaviour
{
    private void OnEnable()
    {
        if (GameManager.instance.gameMode == GameManager.GameMode.Practice)
            this.GetComponent<Text>().text = "PRACTICE";
        else
            this.GetComponent<Text>().text = LevelManager.instance.getDrillsLeftMessage();
    }

    private void OnDisable()
    {
        if (gameObject.activeSelf)
            gameObject.SetActive(false);
    }
}