﻿using OpenCvSharp.Demo;
using UnityEngine;
using UnityEngine.UI;

namespace OpenCvSharp.Demo
{
    using UnityEngine;
    using System.Collections;

    public enum ScaleType
    {
        WIDTH = 0, HEIGHT
    }
    /// <summary>
    /// This script scale surface with WebCameraTexuture to match the screen size
    /// </summary>
    public class CameraScaler : MonoBehaviour
    {
        public ScaleType scaleBy = ScaleType.WIDTH;

        private Vector2 ScreenSize
        {
            get;
            set;
        }

        private Vector2 ComponentSize
        {
            get;
            set;
        }

        void Start()
        {
            ScreenSize = Vector2.zero;
            ComponentSize = Vector2.zero;
            Update();
        }

        void Update()
        {
            Vector2 compontSize = this.GetComponent<RectTransform>().sizeDelta;
            if (Screen.width != ScreenSize.x || Screen.height != ScreenSize.y ||
                compontSize.x != ComponentSize.x || compontSize.y != ComponentSize.y ||
                compontSize.x != transform.parent.GetComponent<RectTransform>().sizeDelta.x ||
                compontSize.y != transform.parent.GetComponent<RectTransform>().sizeDelta.y)
            {
                ScreenSize = new Vector2(Screen.width, Screen.height);
                ComponentSize = compontSize;

                Scale();
            }
        }

        void Scale()
        {
            float width = ComponentSize.x;
            float height = ComponentSize.y;
            if (width <= 0 || height <= 0 || Screen.width <= 0 || Screen.height <= 0)
            {
                return;
            }

            float aspect = Mathf.Min(Screen.height / height, Screen.width / width);
            //this.transform.localScale = new Vector3(aspect, aspect, 1.0f);
            if (this.GetComponent<RawImage>() != null && this.GetComponent<RawImage>().texture != null)
            {
                //Debug.Log("sizing...");
                this.GetComponent<RawImage>().SizeToParent();
            }
        }
    }
}
public static class CanvasExtensions
{
    public static Vector2 SizeToParent(this RawImage image, float padding = 0, ScaleType scaleBy = ScaleType.WIDTH)
    {
        var parent = image.transform.parent.GetComponent<RectTransform>();
        var imageTransform = image.GetComponent<RectTransform>();
        if (!parent) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
        padding = 1 - padding;
        float w = 0, h = 0;
        float ratio = image.texture.width / (float)image.texture.height;
        var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
        if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
        {
            //Invert the bounds if the image is rotated
            bounds.size = new Vector2(bounds.height, bounds.width);
        }
        //Size by height first
        if (scaleBy == ScaleType.HEIGHT)
        {
            h = bounds.height * padding;
            w = h / ratio;
        }
        else /*(w > bounds.width * padding)*/
        { //If it doesn't fit, fallback to width;
            w = bounds.width * padding;
            h = w / ratio;
        }
        imageTransform.sizeDelta = new Vector2(w, h);
        return imageTransform.sizeDelta;
    }
}