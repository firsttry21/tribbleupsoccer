﻿namespace OpenCvSharp.Demo
{
    using UnityEngine;
    using System.Collections;
    using OpenCvSharp;
    using UnityEngine.UI;

    public class ContoursByShapeScript : MonoBehaviour
    {
        public Texture2D texture;

        [Header("Color Lower Threshold")]
        [Range(0f, 255f)]
        public float lh = 22f;
        [Range(0f, 255f)]
        public float ls = 54f;
        [Range(0f, 255f)]
        public float lv = 154f;
        [Header("Color Upper Threshold")]
        [Range(0f, 255f)]
        public float uh = 32f;
        [Range(0f, 255f)]
        public float us = 255f;
        [Range(0f, 255f)]
        public float uv = 255f;

        Mat image;
        Scalar lowerYellow;
        Scalar upperYellow;

        private void Start()
        {
            //Load texture
            image = Unity.TextureToMat(this.texture);

            lowerYellow = new Scalar(lh, ls, lv);
            upperYellow = new Scalar(uh, us, uv);
        }

        void OnEnable()
        {
            if (image != null)
            {
                lowerYellow = new Scalar(lh, ls, lv);
                upperYellow = new Scalar(uh, us, uv);
                // Clean up image using Gaussian Blur
                Mat blurred = new Mat();
                Cv2.GaussianBlur(image, blurred, new Size(5, 5), 0);
                //Convert image to hsv
                Mat hsv = new Mat();
                Cv2.CvtColor(blurred, hsv, ColorConversionCodes.BGR2HSV);
                //construct a mask for the color "yellow", then perform
                //a series of dilations and erosions to remove any small
                //blobs left in the mask
                Mat mask = new Mat();
                Cv2.InRange(hsv, lowerYellow, upperYellow, mask);
                Cv2.Erode(mask, mask, new Mat(), null, 2);
                Cv2.Dilate(mask, mask, new Mat(), null, 2);

                //// Extract Contours
                //Point[][] contours;
                //HierarchyIndex[] hierarchy;
                //Cv2.FindContours(thresh, out contours, out hierarchy, RetrievalModes.Tree, ContourApproximationModes.ApproxNone, null);

                //foreach (Point[] contour in contours)
                //{
                //    double length = Cv2.ArcLength(contour, true);
                //    Point[] approx = Cv2.ApproxPolyDP(contour, length * 0.01, true);
                //    string shapeName = null;
                //    Scalar color = new Scalar();

                //    if (approx.Length == 3)
                //    {
                //        shapeName = "Triangle";
                //        color = new Scalar(0, 255, 0);
                //    }
                //    else if (approx.Length == 4)
                //    {
                //        OpenCvSharp.Rect rect = Cv2.BoundingRect(contour);
                //        if (rect.Width / rect.Height <= 0.1)
                //        {
                //            shapeName = "Square";
                //            color = new Scalar(0, 125, 255);
                //        }
                //        else
                //        {
                //            shapeName = "Rectangle";
                //            color = new Scalar(0, 0, 255);
                //        }
                //    }
                //    else if (approx.Length == 10)
                //    {
                //        shapeName = "Star";
                //        color = new Scalar(255, 255, 0);
                //    }
                //    else if (approx.Length >= 15)
                //    {
                //        shapeName = "Circle";
                //        color = new Scalar(0, 255, 255);
                //    }

                //    if (shapeName != null)
                //    {
                //        Moments m = Cv2.Moments(contour);
                //        int cx = (int)(m.M10 / m.M00);
                //        int cy = (int)(m.M01 / m.M00);

                //        Cv2.DrawContours(image, new Point[][] { contour }, 0, color, -1);
                //        Cv2.PutText(image, shapeName, new Point(cx - 50, cy), HersheyFonts.HersheySimplex, 1.0, new Scalar(0, 0, 0));
                //    }
                //}


                // Render texture
                Texture2D texture = Unity.MatToTexture(mask);
                RawImage rawImage = gameObject.GetComponent<RawImage>();
                rawImage.texture = texture;

            }

        }
    }
}